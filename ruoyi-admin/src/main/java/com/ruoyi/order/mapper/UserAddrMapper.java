package com.ruoyi.order.mapper;

import java.util.List;
import com.ruoyi.order.domain.UserAddr;
import org.springframework.data.repository.query.Param;

/**
 * 用户配送地址Mapper接口
 * 
 * @author ruoyi
 * @date 2024-01-14
 */
public interface UserAddrMapper 
{

 /**
     * 查询用户配送地址
     *
     * @param addrId 用户配送地址主键
     * @return 用户配送地址
     */
    public UserAddr selectUserAddrByAddrId(Long addrId);

    /**
     * 查询用户配送地址列表
     * 
     * @param userAddr 用户配送地址
     * @return 用户配送地址集合
     */
    public List<UserAddr> selectUserAddrList(UserAddr userAddr);

    /**
     * 新增用户配送地址
     * 
     * @param userAddr 用户配送地址
     * @return 结果
     */
    public int insertUserAddr(UserAddr userAddr);

    /**
     * 修改用户配送地址
     * 
     * @param userAddr 用户配送地址
     * @return 结果
     */
    public int updateUserAddr(UserAddr userAddr);

    /**
     * 删除用户配送地址
     * 
     * @param addrId 用户配送地址主键
     * @return 结果
     */
    public int deleteUserAddrByAddrId(Long addrId);

    /**
     * 批量删除用户配送地址
     * 
     * @param addrIds 需要删除的数据主键集合
     * @return 结果
     */
    public int deleteUserAddrByAddrIds(Long[] addrIds);
   /**
    * 根据用户id和地址id获取地址
    * @param userId
    * @param addrId
    * @return
    */
   UserAddr getUserAddrByUserIdAndAddrId(@Param("userId") String userId, @Param("addrId") Long addrId);
   /**
    * 根据用户id获取默认地址
    * @param userId
    * @return
    */
   UserAddr getDefaultUserAddr(@Param("userId") String userId);
}
