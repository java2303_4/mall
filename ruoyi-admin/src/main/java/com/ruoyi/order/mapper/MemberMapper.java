package com.ruoyi.order.mapper;

import java.util.List;

import com.ruoyi.common.core.domain.entity.SysUser;
import com.ruoyi.order.domain.Member;

/**
 * 会员管理Mapper接口
 * 
 * @author ruoyi
 * @date 2024-01-17
 */
public interface MemberMapper 
{
    /**
     * 查询会员管理
     * 
     * @param memberId 会员管理主键
     * @return 会员管理
     */
    public Member selectMemberByMemberId(Long memberId);

    /**
     * 查询会员管理列表
     * 
     * @param member 会员管理
     * @return 会员管理集合
     */
    public List<Member> selectMemberList(Member member);

    /**
     * 新增会员管理
     * 
     * @param member 会员管理
     * @return 结果
     */
    public int insertMember(Member member);

    /**
     * 修改会员管理
     * 
     * @param member 会员管理
     * @return 结果
     */
    public int updateMember(Member member);

    /**
     * 删除会员管理
     * 
     * @param memberId 会员管理主键
     * @return 结果
     */
    public int deleteMemberByMemberId(Long memberId);

    /**
     * 批量删除会员管理
     * 
     * @param memberIds 需要删除的数据主键集合
     * @return 结果
     */
    public int deleteMemberByMemberIds(Long[] memberIds);

    /**
     * 批量删除用户信息
     * 
     * @param memberIds 需要删除的数据主键集合
     * @return 结果
     */
    public int deleteSysUserByUserIds(Long[] memberIds);
    
    /**
     * 批量新增用户信息
     * 
     * @param sysUserList 用户信息列表
     * @return 结果
     */
    public int batchSysUser(List<SysUser> sysUserList);
    

    /**
     * 通过会员管理主键删除用户信息信息
     * 
     * @param memberId 会员管理ID
     * @return 结果
     */
    public int deleteSysUserByUserId(Long memberId);
}
