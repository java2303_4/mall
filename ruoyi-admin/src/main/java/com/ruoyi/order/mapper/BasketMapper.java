package com.ruoyi.order.mapper;

import java.util.List;
import java.util.Map;

import com.ruoyi.app.dto.ShopCartItemDto;
import com.ruoyi.app.param.ShopCartParam;
import com.ruoyi.order.domain.Basket;
import org.springframework.data.repository.query.Param;

/**
 * 购物车Mapper接口
 * 
 * @author ruoyi
 * @date 2024-01-14
 */
public interface BasketMapper 
{
    /**
     * 查询购物车
     * 
     * @param basketId 购物车主键
     * @return 购物车
     */
    public Basket selectBasketByBasketId(Long basketId);

    /**
     * 查询购物车列表
     * 
     * @param basket 购物车
     * @return 购物车集合
     */
    public List<Basket> selectBasketList(Basket basket);

    /**
     * 新增购物车
     * 
     * @param basket 购物车
     * @return 结果
     */
    public int insertBasket(Basket basket);

    /**
     * 修改购物车
     * 
     * @param basket 购物车
     * @return 结果
     */
    public int updateBasket(Basket basket);

    /**
     * 删除购物车
     * 
     * @param basketId 购物车主键
     * @return 结果
     */
    public int deleteBasketByBasketId(Long basketId);

    /**
     * 批量删除购物车
     * 
     * @param basketIds 需要删除的数据主键集合
     * @return 结果
     */
    public int deleteBasketByBasketIds(Long[] basketIds);
    /**
     * 更新购物车满减活动id
     * @param userId 用户id
     * @param basketIdShopCartParamMap 购物项map
     */
    void updateDiscountItemId(@Param("userId")String userId, @Param("basketIdShopCartParamMap") Map<Long, ShopCartParam> basketIdShopCartParamMap);
    /**
     * 获取购物项
     * @param userId 用户id
     * @return 购物项列表
     */
    List<ShopCartItemDto> getShopCartItems(@Param("userId") String userId);
    /**
     * 根据购物车id列表和用户id删除购物车
     * @param userId 用户id
     * @param basketIds 购物车id列表
     */
    void deleteShopCartItemsByBasketIds(@Param("userId") String userId, @Param("basketIds") List<Long> basketIds);

    /**
     * 删除所有购物车
     * @param userId 用户id
     */
    void deleteAllShopCartItems(@Param("userId") String userId);
    /**
     * 获取失效的购物项
     * @param userId 用户id
     * @return 失效的购物项
     */
    List<ShopCartItemDto> getShopCartExpiryItems(@Param("userId") String userId);
    /**
     * 删除失效的购物项
     * @param userId 用户id
     */
    void cleanExpiryProdList(@Param("userId") String userId);
}
