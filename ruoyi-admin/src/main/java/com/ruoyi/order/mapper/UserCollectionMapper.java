package com.ruoyi.order.mapper;

import java.util.List;
import com.ruoyi.order.domain.UserCollection;

/**
 * 收藏Mapper接口
 * 
 * @author ruoyi
 * @date 2024-01-11
 */
public interface UserCollectionMapper 
{
    /**
     * 查询收藏
     * 
     * @param id 收藏主键
     * @return 收藏
     */
    public UserCollection selectUserCollectionById(Long id);

    /**
     * 查询收藏列表
     * 
     * @param userCollection 收藏
     * @return 收藏集合
     */
    public List<UserCollection> selectUserCollectionList(UserCollection userCollection);

    /**
     * 新增收藏
     * 
     * @param userCollection 收藏
     * @return 结果
     */
    public int insertUserCollection(UserCollection userCollection);

    /**
     * 修改收藏
     * 
     * @param userCollection 收藏
     * @return 结果
     */
    public int updateUserCollection(UserCollection userCollection);

    /**
     * 删除收藏
     * 
     * @param id 收藏主键
     * @return 结果
     */
    public int deleteUserCollectionById(Long id);

    /**
     * 批量删除收藏
     * 
     * @param ids 需要删除的数据主键集合
     * @return 结果
     */
    public int deleteUserCollectionByIds(Long[] ids);
}
