package com.ruoyi.order.service.impl;

import java.util.List;

import com.ruoyi.common.core.domain.entity.SysUser;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import java.util.ArrayList;
import com.ruoyi.common.utils.StringUtils;
import org.springframework.transaction.annotation.Transactional;
import com.ruoyi.order.mapper.MemberMapper;
import com.ruoyi.order.domain.Member;
import com.ruoyi.order.service.IMemberService;

/**
 * 会员管理Service业务层处理
 * 
 * @author ruoyi
 * @date 2024-01-17
 */
@Service
public class MemberServiceImpl implements IMemberService 
{
    @Autowired
    private MemberMapper memberMapper;

    /**
     * 查询会员管理
     * 
     * @param memberId 会员管理主键
     * @return 会员管理
     */
    @Override
    public Member selectMemberByMemberId(Long memberId)
    {
        return memberMapper.selectMemberByMemberId(memberId);
    }

    /**
     * 查询会员管理列表
     * 
     * @param member 会员管理
     * @return 会员管理
     */
    @Override
    public List<Member> selectMemberList(Member member)
    {
        return memberMapper.selectMemberList(member);
    }

    /**
     * 新增会员管理
     * 
     * @param member 会员管理
     * @return 结果
     */
    @Transactional
    @Override
    public int insertMember(Member member)
    {
        int rows = memberMapper.insertMember(member);
        insertSysUser(member);
        return rows;
    }

    /**
     * 修改会员管理
     * 
     * @param member 会员管理
     * @return 结果
     */
    @Transactional
    @Override
    public int updateMember(Member member)
    {
        memberMapper.deleteSysUserByUserId(member.getUserId());
        insertSysUser(member);
        return memberMapper.updateMember(member);
    }

    /**
     * 批量删除会员管理
     * 
     * @param memberIds 需要删除的会员管理主键
     * @return 结果
     */
    @Transactional
    @Override
    public int deleteMemberByMemberIds(Long[] memberIds)
    {
        memberMapper.deleteSysUserByUserIds(memberIds);
        return memberMapper.deleteMemberByMemberIds(memberIds);
    }

    /**
     * 删除会员管理信息
     * 
     * @param memberId 会员管理主键
     * @return 结果
     */
    @Transactional
    @Override
    public int deleteMemberByMemberId(Long memberId)
    {
        memberMapper.deleteSysUserByUserId(memberId);
        return memberMapper.deleteMemberByMemberId(memberId);
    }

    /**
     * 新增用户信息信息
     * 
     * @param member 会员管理对象
     */
    public void insertSysUser(Member member)
    {
        List<SysUser> sysUserList = member.getSysUserList();
        Long memberId = member.getUserId();
        if (StringUtils.isNotNull(sysUserList))
        {
            List<SysUser> list = new ArrayList<SysUser>();
            for (SysUser sysUser : sysUserList)
            {
                sysUser.setUserId(memberId);
                list.add(sysUser);
            }
            if (list.size() > 0)
            {
                memberMapper.batchSysUser(list);
            }
        }
    }
}
