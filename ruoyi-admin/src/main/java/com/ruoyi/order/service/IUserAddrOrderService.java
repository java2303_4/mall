package com.ruoyi.order.service;

import java.util.List;
import com.ruoyi.order.domain.UserAddrOrder;

/**
 * 用户订单配送地址Service接口
 * 
 * @author ruoyi
 * @date 2024-01-14
 */
public interface IUserAddrOrderService 
{
    /**
     * 查询用户订单配送地址
     * 
     * @param addrOrderId 用户订单配送地址主键
     * @return 用户订单配送地址
     */
    public UserAddrOrder selectUserAddrOrderByAddrOrderId(Long addrOrderId);

    /**
     * 查询用户订单配送地址列表
     * 
     * @param userAddrOrder 用户订单配送地址
     * @return 用户订单配送地址集合
     */
    public List<UserAddrOrder> selectUserAddrOrderList(UserAddrOrder userAddrOrder);

    /**
     * 新增用户订单配送地址
     * 
     * @param userAddrOrder 用户订单配送地址
     * @return 结果
     */
    public int insertUserAddrOrder(UserAddrOrder userAddrOrder);

    /**
     * 修改用户订单配送地址
     * 
     * @param userAddrOrder 用户订单配送地址
     * @return 结果
     */
    public int updateUserAddrOrder(UserAddrOrder userAddrOrder);

    /**
     * 批量删除用户订单配送地址
     * 
     * @param addrOrderIds 需要删除的用户订单配送地址主键集合
     * @return 结果
     */
    public int deleteUserAddrOrderByAddrOrderIds(Long[] addrOrderIds);

    /**
     * 删除用户订单配送地址信息
     * 
     * @param addrOrderId 用户订单配送地址主键
     * @return 结果
     */
    public int deleteUserAddrOrderByAddrOrderId(Long addrOrderId);
}
