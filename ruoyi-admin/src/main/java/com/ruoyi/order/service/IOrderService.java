package com.ruoyi.order.service;

import java.util.List;

import com.ruoyi.app.dto.ShopCartOrderMergerDto;
import com.ruoyi.order.domain.Order;
import com.ruoyi.order.domain.OrderCount;

/**
 * 订单Service接口
 * 
 * @author ruoyi
 * @date 2024-01-03
 */
public interface IOrderService 
{
    /**
     * 查询订单
     * 
     * @param orderId 订单主键
     * @return 订单
     */
    public Order selectOrderByOrderId(Long orderId);

    /**
     * 查询订单列表
     * 
     * @param order 订单
     * @return 订单集合
     */
    public List<Order> selectOrderList(Order order);

    /**
     * 新增订单
     * 
     * @param order 订单
     * @return 结果
     */
    public int insertOrder(Order order);

    /**
     * 修改订单
     * 
     * @param order 订单
     * @return 结果
     */
    public int updateOrder(Order order);

    /**
     * 批量删除订单
     * 
     * @param orderIds 需要删除的订单主键集合
     * @return 结果
     */
    public int deleteOrderByOrderIds(Long[] orderIds);

    /**
     * 删除订单信息
     * 
     * @param orderId 订单主键
     * @return 结果
     */
    public int deleteOrderByOrderId(Long orderId);

    OrderCount selectOrderByMemberId(Long userId);
    /**
     * 新增订单缓存
     * @param userId
     * @param shopCartOrderMergerDto
     * @return
     */
    ShopCartOrderMergerDto putConfirmOrderCache(String userId , ShopCartOrderMergerDto shopCartOrderMergerDto);
}
