package com.ruoyi.order.service;

import java.util.List;
import com.ruoyi.order.domain.UserAddr;

/**
 * 用户配送地址Service接口
 * 
 * @author ruoyi
 * @date 2024-01-14
 */
public interface IUserAddrService 
{
    /**
     * 查询用户配送地址
     * 
     * @param addrId 用户配送地址主键
     * @return 用户配送地址
     */
    public UserAddr selectUserAddrByAddrId(Long addrId);

    /**
     * 查询用户配送地址列表
     * 
     * @param userAddr 用户配送地址
     * @return 用户配送地址集合
     */
    public List<UserAddr> selectUserAddrList(UserAddr userAddr);

    /**
     * 新增用户配送地址
     * 
     * @param userAddr 用户配送地址
     * @return 结果
     */
    public int insertUserAddr(UserAddr userAddr);

    /**
     * 修改用户配送地址
     * 
     * @param userAddr 用户配送地址
     * @return 结果
     */
    public int updateUserAddr(UserAddr userAddr);

    /**
     * 批量删除用户配送地址
     * 
     * @param addrIds 需要删除的用户配送地址主键集合
     * @return 结果
     */
    public int deleteUserAddrByAddrIds(Long[] addrIds);

    /**
     * 删除用户配送地址信息
     * 
     * @param addrId 用户配送地址主键
     * @return 结果
     */
    public int deleteUserAddrByAddrId(Long addrId);
    /**
     * 根据用户id和地址id获取用户地址
     * @param addrId
     * @param userId
     * @return
     */
    UserAddr getUserAddrByUserId(Long addrId, String userId);
}
