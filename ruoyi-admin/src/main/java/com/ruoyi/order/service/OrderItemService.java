package com.ruoyi.order.service;

import com.ruoyi.order.domain.OrderItem;
import java.util.List;
/**
 * 订单项(OrderItem)表服务接口
 *
 * @author makejava
 * @since 2024-01-07 15:06:37
 */
public interface OrderItemService {

    /**
     * 通过ID查询单条数据
     *
     * @param orderItemId 主键
     * @return 实例对象
     */
    OrderItem queryById(Long orderItemId);

    /**
     * 分页查询
     *
     * @param orderItem 筛选条件
     * @return 查询结果
     */
    List<OrderItem> queryByPage(OrderItem orderItem);

    /**
     * 新增数据
     *
     * @param orderItem 实例对象
     * @return 实例对象
     */
    OrderItem insert(OrderItem orderItem);

    /**
     * 修改数据
     *
     * @param orderItem 实例对象
     * @return 实例对象
     */
    OrderItem update(OrderItem orderItem);

    /**
     * 通过主键删除数据
     *
     * @param orderItemId 主键
     * @return 是否成功
     */
    boolean deleteById(Long orderItemId);

}
