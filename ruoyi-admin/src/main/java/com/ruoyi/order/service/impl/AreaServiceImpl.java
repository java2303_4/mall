package com.ruoyi.order.service.impl;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ruoyi.order.mapper.AreaMapper;
import com.ruoyi.order.domain.Area;
import com.ruoyi.order.service.IAreaService;

/**
 * 地址管理Service业务层处理
 * 
 * @author ruoyi
 * @date 2024-01-03
 */
@Service
public class AreaServiceImpl implements IAreaService 
{
    @Autowired
    private AreaMapper areaMapper;

    /**
     * 查询地址管理
     * 
     * @param areaId 地址管理主键
     * @return 地址管理
     */
    @Override
    public Area selectAreaByAreaId(Long areaId)
    {
        return areaMapper.selectAreaByAreaId(areaId);
    }

    /**
     * 查询地址管理列表
     * 
     * @param area 地址管理
     * @return 地址管理
     */
    @Override
    public List<Area> selectAreaList(Area area)
    {
        return areaMapper.selectAreaList(area);
    }

    /**
     * 新增地址管理
     * 
     * @param area 地址管理
     * @return 结果
     */
    @Override
    public int insertArea(Area area)
    {
        return areaMapper.insertArea(area);
    }

    /**
     * 修改地址管理
     * 
     * @param area 地址管理
     * @return 结果
     */
    @Override
    public int updateArea(Area area)
    {
        return areaMapper.updateArea(area);
    }

    /**
     * 批量删除地址管理
     * 
     * @param areaIds 需要删除的地址管理主键
     * @return 结果
     */
    @Override
    public int deleteAreaByAreaIds(Long[] areaIds)
    {
        return areaMapper.deleteAreaByAreaIds(areaIds);
    }

    /**
     * 删除地址管理信息
     * 
     * @param areaId 地址管理主键
     * @return 结果
     */
    @Override
    public int deleteAreaByAreaId(Long areaId)
    {
        return areaMapper.deleteAreaByAreaId(areaId);
    }
}
