package com.ruoyi.order.service.impl;

import java.util.List;
import com.ruoyi.common.utils.DateUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ruoyi.order.mapper.UserCollectionMapper;
import com.ruoyi.order.domain.UserCollection;
import com.ruoyi.order.service.IUserCollectionService;

/**
 * 收藏Service业务层处理
 * 
 * @author ruoyi
 * @date 2024-01-11
 */
@Service
public class UserCollectionServiceImpl implements IUserCollectionService 
{
    @Autowired
    private UserCollectionMapper userCollectionMapper;

    /**
     * 查询收藏
     * 
     * @param id 收藏主键
     * @return 收藏
     */
    @Override
    public UserCollection selectUserCollectionById(Long id)
    {
        return userCollectionMapper.selectUserCollectionById(id);
    }

    /**
     * 查询收藏列表
     * 
     * @param userCollection 收藏
     * @return 收藏
     */
    @Override
    public List<UserCollection> selectUserCollectionList(UserCollection userCollection)
    {
        return userCollectionMapper.selectUserCollectionList(userCollection);
    }

    /**
     * 新增收藏
     * 
     * @param userCollection 收藏
     * @return 结果
     */
    @Override
    public int insertUserCollection(UserCollection userCollection)
    {
        userCollection.setCreateTime(DateUtils.getNowDate());
        return userCollectionMapper.insertUserCollection(userCollection);
    }

    /**
     * 修改收藏
     * 
     * @param userCollection 收藏
     * @return 结果
     */
    @Override
    public int updateUserCollection(UserCollection userCollection)
    {
        return userCollectionMapper.updateUserCollection(userCollection);
    }

    /**
     * 批量删除收藏
     * 
     * @param ids 需要删除的收藏主键
     * @return 结果
     */
    @Override
    public int deleteUserCollectionByIds(Long[] ids)
    {
        return userCollectionMapper.deleteUserCollectionByIds(ids);
    }

    /**
     * 删除收藏信息
     * 
     * @param id 收藏主键
     * @return 结果
     */
    @Override
    public int deleteUserCollectionById(Long id)
    {
        return userCollectionMapper.deleteUserCollectionById(id);
    }
}
