package com.ruoyi.order.service.impl;

import java.util.List;
import com.ruoyi.common.utils.DateUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ruoyi.order.mapper.UserAddrMapper;
import com.ruoyi.order.domain.UserAddr;
import com.ruoyi.order.service.IUserAddrService;

/**
 * 用户配送地址Service业务层处理
 * 
 * @author ruoyi
 * @date 2024-01-14
 */
@Service
public class UserAddrServiceImpl implements IUserAddrService 
{
    @Autowired
    private UserAddrMapper userAddrMapper;

    /**
     * 查询用户配送地址
     * 
     * @param addrId 用户配送地址主键
     * @return 用户配送地址
     */
    @Override
    public UserAddr selectUserAddrByAddrId(Long addrId)
    {
        return userAddrMapper.selectUserAddrByAddrId(addrId);
    }

    /**
     * 查询用户配送地址列表
     * 
     * @param userAddr 用户配送地址
     * @return 用户配送地址
     */
    @Override
    public List<UserAddr> selectUserAddrList(UserAddr userAddr)
    {
        return userAddrMapper.selectUserAddrList(userAddr);
    }

    /**
     * 新增用户配送地址
     * 
     * @param userAddr 用户配送地址
     * @return 结果
     */
    @Override
    public int insertUserAddr(UserAddr userAddr)
    {
        userAddr.setCreateTime(DateUtils.getNowDate());
        return userAddrMapper.insertUserAddr(userAddr);
    }

    /**
     * 修改用户配送地址
     * 
     * @param userAddr 用户配送地址
     * @return 结果
     */
    @Override
    public int updateUserAddr(UserAddr userAddr)
    {
        userAddr.setUpdateTime(DateUtils.getNowDate());
        return userAddrMapper.updateUserAddr(userAddr);
    }

    /**
     * 批量删除用户配送地址
     * 
     * @param addrIds 需要删除的用户配送地址主键
     * @return 结果
     */
    @Override
    public int deleteUserAddrByAddrIds(Long[] addrIds)
    {
        return userAddrMapper.deleteUserAddrByAddrIds(addrIds);
    }

    /**
     * 删除用户配送地址信息
     * 
     * @param addrId 用户配送地址主键
     * @return 结果
     */
    @Override
    public int deleteUserAddrByAddrId(Long addrId)
    {
        return userAddrMapper.deleteUserAddrByAddrId(addrId);
    }
    @Override
    public UserAddr getUserAddrByUserId(Long addrId, String userId) {
        if (addrId == 0) {
            return userAddrMapper.getDefaultUserAddr(userId);
        }
        return userAddrMapper.getUserAddrByUserIdAndAddrId(userId, addrId);
    }
}
