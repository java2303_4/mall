package com.ruoyi.order.service.impl;

import com.ruoyi.order.domain.OrderItem;
import com.ruoyi.order.mapper.OrderItemMapper;
import com.ruoyi.order.service.OrderItemService;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;

/**
 * 订单项(OrderItem)表服务实现类
 *
 * @author makejava
 * @since 2024-01-07 15:06:37
 */
@Service("orderItemService")
public class OrderItemServiceImpl implements OrderItemService {
    @Resource
    private OrderItemMapper orderItemDao;

    /**
     * 通过ID查询单条数据
     *
     * @param orderItemId 主键
     * @return 实例对象
     */
    @Override
    public OrderItem queryById(Long orderItemId) {
        return this.orderItemDao.queryById(orderItemId);
    }

    /**
     * 分页查询
     *
     * @param orderItem 筛选条件
     * @return 查询结果
     */
    @Override
    public List<OrderItem> queryByPage(OrderItem orderItem) {
        return this.orderItemDao.queryAll(orderItem);
    }

    /**
     * 新增数据
     *
     * @param orderItem 实例对象
     * @return 实例对象
     */
    @Override
    public OrderItem insert(OrderItem orderItem) {
        this.orderItemDao.insert(orderItem);
        return orderItem;
    }

    /**
     * 修改数据
     *
     * @param orderItem 实例对象
     * @return 实例对象
     */
    @Override
    public OrderItem update(OrderItem orderItem) {
        this.orderItemDao.update(orderItem);
        return this.queryById(orderItem.getOrderItemId());
    }

    /**
     * 通过主键删除数据
     *
     * @param orderItemId 主键
     * @return 是否成功
     */
    @Override
    public boolean deleteById(Long orderItemId) {
        return this.orderItemDao.deleteById(orderItemId) > 0;
    }
}
