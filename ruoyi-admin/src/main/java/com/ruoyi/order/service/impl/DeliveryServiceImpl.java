package com.ruoyi.order.service.impl;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ruoyi.order.mapper.DeliveryMapper;
import com.ruoyi.order.domain.Delivery;
import com.ruoyi.order.service.IDeliveryService;

/**
 * 物流公司Service业务层处理
 * 
 * @author ruoyi
 * @date 2024-01-15
 */
@Service
public class DeliveryServiceImpl implements IDeliveryService 
{
    @Autowired
    private DeliveryMapper deliveryMapper;

    /**
     * 查询物流公司
     * 
     * @param dvyId 物流公司主键
     * @return 物流公司
     */
    @Override
    public Delivery selectDeliveryByDvyId(Long dvyId)
    {
        return deliveryMapper.selectDeliveryByDvyId(dvyId);
    }

    /**
     * 查询物流公司列表
     * 
     * @param delivery 物流公司
     * @return 物流公司
     */
    @Override
    public List<Delivery> selectDeliveryList(Delivery delivery)
    {
        return deliveryMapper.selectDeliveryList(delivery);
    }

    /**
     * 新增物流公司
     * 
     * @param delivery 物流公司
     * @return 结果
     */
    @Override
    public int insertDelivery(Delivery delivery)
    {
        return deliveryMapper.insertDelivery(delivery);
    }

    /**
     * 修改物流公司
     * 
     * @param delivery 物流公司
     * @return 结果
     */
    @Override
    public int updateDelivery(Delivery delivery)
    {
        return deliveryMapper.updateDelivery(delivery);
    }

    /**
     * 批量删除物流公司
     * 
     * @param dvyIds 需要删除的物流公司主键
     * @return 结果
     */
    @Override
    public int deleteDeliveryByDvyIds(Long[] dvyIds)
    {
        return deliveryMapper.deleteDeliveryByDvyIds(dvyIds);
    }

    /**
     * 删除物流公司信息
     * 
     * @param dvyId 物流公司主键
     * @return 结果
     */
    @Override
    public int deleteDeliveryByDvyId(Long dvyId)
    {
        return deliveryMapper.deleteDeliveryByDvyId(dvyId);
    }
}
