package com.ruoyi.order.service;

import java.util.List;
import java.util.Map;

import com.ruoyi.app.dto.ShopCartDto;
import com.ruoyi.app.dto.ShopCartItemDto;
import com.ruoyi.app.param.ChangeShopCartParam;
import com.ruoyi.app.param.OrderItemParam;
import com.ruoyi.app.param.ShopCartParam;
import com.ruoyi.order.domain.Basket;

/**
 * 购物车Service接口
 * 
 * @author ruoyi
 * @date 2024-01-14
 */
public interface IBasketService 
{
    /**
     * 查询购物车
     * 
     * @param basketId 购物车主键
     * @return 购物车
     */
    public Basket selectBasketByBasketId(Long basketId);

    /**
     * 查询购物车列表
     * 
     * @param basket 购物车
     * @return 购物车集合
     */
    public List<Basket> selectBasketList(Basket basket);

    /**
     * 新增购物车
     * 
     * @param basket 购物车
     * @return 结果
     */
    public int insertBasket(Basket basket);

    /**
     * 修改购物车
     * 
     * @param basket 购物车
     * @return 结果
     */
    public int updateBasket(Basket basket);

    /**
     * 批量删除购物车
     * 
     * @param basketIds 需要删除的购物车主键集合
     * @return 结果
     */
    public int deleteBasketByBasketIds(Long[] basketIds);

    /**
     * 删除购物车信息
     * 
     * @param basketId 购物车主键
     * @return 结果
     */
    public int deleteBasketByBasketId(Long basketId);
    /**
     * 组装获取用户提交的购物车商品项
     * @param orderItem 提交订单时携带的商品信息
     * @param userId 当前用户的用户id
     * @param basketId 购物车id
     * @return 所有的商品购物项
     */
    List<ShopCartItemDto> getShopCartItemsByOrderItems(List<Long> basketId, OrderItemParam orderItem, String userId);
    /**
     * 根据店铺组装购车中的商品信息，返回每个店铺中的购物车商品信息
     * @param shopCartItems 指定的购物项目
     * @return 每个店铺的购物车信息
     */
    List<ShopCartDto> getShopCarts(List<ShopCartItemDto> shopCartItems);
    /**
     * 更新满减活动id
     * @param userId 用户id
     * @param basketIdShopCartParamMap 购物车map
     */
    void updateBasketByShopCartParam(String userId, Map<Long, ShopCartParam> basketIdShopCartParamMap);
    /**
     * 根据用户id获取购物车列表
     * @param userId 用户id
     * @return 购物车列表
     */
    List<ShopCartItemDto> getShopCartItems(String userId);
    /**
     * 根据购物车id列表删除购物项
     * @param userId 用户id
     * @param basketIds 购物车id列表
     */
    void deleteShopCartItemsByBasketIds(String userId, List<Long> basketIds);

    /**
     * 删除所有购物车
     * @param userId
     */
    void deleteAllShopCartItems(String userId);
    /**
     * 更新购物车
     * @param basket
     */
    void updateShopCartItem(Basket basket);
    /**
     * 添加购物车
     * @param param
     * @param userId
     */
    void addShopCartItem(ChangeShopCartParam param, String userId);
    /**
     * 获取购物车失效列表
     * @param userId 用户id
     * @return 失效商品
     */
    List<ShopCartItemDto> getShopCartExpiryItems(String userId);

    /**
     * 清除失效的购物项
     * @param userId 用户id
     */
    void cleanExpiryProdList(String userId);

}
