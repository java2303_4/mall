package com.ruoyi.order.service.impl;

import java.util.List;
import com.ruoyi.common.utils.DateUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ruoyi.order.mapper.UserAddrOrderMapper;
import com.ruoyi.order.domain.UserAddrOrder;
import com.ruoyi.order.service.IUserAddrOrderService;

/**
 * 用户订单配送地址Service业务层处理
 * 
 * @author ruoyi
 * @date 2024-01-14
 */
@Service
public class UserAddrOrderServiceImpl implements IUserAddrOrderService 
{
    @Autowired
    private UserAddrOrderMapper userAddrOrderMapper;

    /**
     * 查询用户订单配送地址
     * 
     * @param addrOrderId 用户订单配送地址主键
     * @return 用户订单配送地址
     */
    @Override
    public UserAddrOrder selectUserAddrOrderByAddrOrderId(Long addrOrderId)
    {
        return userAddrOrderMapper.selectUserAddrOrderByAddrOrderId(addrOrderId);
    }

    /**
     * 查询用户订单配送地址列表
     * 
     * @param userAddrOrder 用户订单配送地址
     * @return 用户订单配送地址
     */
    @Override
    public List<UserAddrOrder> selectUserAddrOrderList(UserAddrOrder userAddrOrder)
    {
        return userAddrOrderMapper.selectUserAddrOrderList(userAddrOrder);
    }

    /**
     * 新增用户订单配送地址
     * 
     * @param userAddrOrder 用户订单配送地址
     * @return 结果
     */
    @Override
    public int insertUserAddrOrder(UserAddrOrder userAddrOrder)
    {
        userAddrOrder.setCreateTime(DateUtils.getNowDate());
        return userAddrOrderMapper.insertUserAddrOrder(userAddrOrder);
    }

    /**
     * 修改用户订单配送地址
     * 
     * @param userAddrOrder 用户订单配送地址
     * @return 结果
     */
    @Override
    public int updateUserAddrOrder(UserAddrOrder userAddrOrder)
    {
        return userAddrOrderMapper.updateUserAddrOrder(userAddrOrder);
    }

    /**
     * 批量删除用户订单配送地址
     * 
     * @param addrOrderIds 需要删除的用户订单配送地址主键
     * @return 结果
     */
    @Override
    public int deleteUserAddrOrderByAddrOrderIds(Long[] addrOrderIds)
    {
        return userAddrOrderMapper.deleteUserAddrOrderByAddrOrderIds(addrOrderIds);
    }

    /**
     * 删除用户订单配送地址信息
     * 
     * @param addrOrderId 用户订单配送地址主键
     * @return 结果
     */
    @Override
    public int deleteUserAddrOrderByAddrOrderId(Long addrOrderId)
    {
        return userAddrOrderMapper.deleteUserAddrOrderByAddrOrderId(addrOrderId);
    }
}
