package com.ruoyi.order.service;

import java.util.List;
import com.ruoyi.order.domain.Area;

/**
 * 地址管理Service接口
 * 
 * @author ruoyi
 * @date 2024-01-03
 */
public interface IAreaService 
{
    /**
     * 查询地址管理
     * 
     * @param areaId 地址管理主键
     * @return 地址管理
     */
    public Area selectAreaByAreaId(Long areaId);

    /**
     * 查询地址管理列表
     * 
     * @param area 地址管理
     * @return 地址管理集合
     */
    public List<Area> selectAreaList(Area area);

    /**
     * 新增地址管理
     * 
     * @param area 地址管理
     * @return 结果
     */
    public int insertArea(Area area);

    /**
     * 修改地址管理
     * 
     * @param area 地址管理
     * @return 结果
     */
    public int updateArea(Area area);

    /**
     * 批量删除地址管理
     * 
     * @param areaIds 需要删除的地址管理主键集合
     * @return 结果
     */
    public int deleteAreaByAreaIds(Long[] areaIds);

    /**
     * 删除地址管理信息
     * 
     * @param areaId 地址管理主键
     * @return 结果
     */
    public int deleteAreaByAreaId(Long areaId);
}
