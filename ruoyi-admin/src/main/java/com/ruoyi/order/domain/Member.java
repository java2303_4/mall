package com.ruoyi.order.domain;

import java.util.List;

import com.ruoyi.common.core.domain.entity.SysUser;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.ruoyi.common.annotation.Excel;
import com.ruoyi.common.core.domain.BaseEntity;

/**
 * 会员管理对象 or_member
 * 
 * @author ruoyi
 * @date 2024-01-17
 */
public class Member extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** 会员ID */
    private Long memberId;

    /** 真实姓名 */
    @Excel(name = "真实姓名")
    private String realName;

    /** 备注 */
    @Excel(name = "备注")
    private String memberMemo;

    /** 状态 */
    @Excel(name = "状态")
    private Integer status;

    /** 用户积分 */
    @Excel(name = "用户积分")
    private Long score;

    /** 用户id */
    private Long userId;

    /** 用户信息信息 */
    private List<SysUser> sysUserList;

    public void setMemberId(Long memberId) 
    {
        this.memberId = memberId;
    }

    public Long getMemberId() 
    {
        return memberId;
    }
    public void setRealName(String realName) 
    {
        this.realName = realName;
    }

    public String getRealName() 
    {
        return realName;
    }
    public void setMemberMemo(String memberMemo) 
    {
        this.memberMemo = memberMemo;
    }

    public String getMemberMemo() 
    {
        return memberMemo;
    }
    public void setStatus(Integer status) 
    {
        this.status = status;
    }

    public Integer getStatus() 
    {
        return status;
    }
    public void setScore(Long score) 
    {
        this.score = score;
    }

    public Long getScore() 
    {
        return score;
    }
    public void setUserId(Long userId) 
    {
        this.userId = userId;
    }

    public Long getUserId() 
    {
        return userId;
    }

    public List<SysUser> getSysUserList()
    {
        return sysUserList;
    }

    public void setSysUserList(List<SysUser> sysUserList)
    {
        this.sysUserList = sysUserList;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("memberId", getMemberId())
            .append("realName", getRealName())
            .append("memberMemo", getMemberMemo())
            .append("status", getStatus())
            .append("score", getScore())
            .append("userId", getUserId())
            .append("sysUserList", getSysUserList())
            .toString();
    }
}
