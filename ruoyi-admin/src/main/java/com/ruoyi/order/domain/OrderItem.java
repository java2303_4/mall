package com.ruoyi.order.domain;

import java.util.Date;
import java.io.Serializable;

/**
 * 订单项(OrderItem)实体类
 *
 * @author makejava
 * @since 2024-01-07 15:06:37
 */

public class OrderItem implements Serializable {
    private static final long serialVersionUID = -43537877714499412L;
    /**
     * 订单项ID
     */
    private Long orderItemId;
    /**
     * 店铺id
     */
    private Long shopId;
    /**
     * 订单order_number
     */
    private String orderNumber;
    /**
     * 产品ID
     */
    private Long prodId;
    /**
     * 产品SkuID
     */
    private Long skuId;
    /**
     * 购物车产品个数
     */
    private Integer prodCount;
    /**
     * 产品名称
     */
    private String prodName;
    /**
     * sku名称
     */
    private String skuName;
    /**
     * 产品主图片路径
     */
    private String pic;
    /**
     * 产品价格
     */
    private Double price;
    /**
     * 用户Id
     */
    private Long memberId;
    /**
     * 商品总金额
     */
    private Double productTotalAmount;
    /**
     * 购物时间
     */
    private Date recTime;
    /**
     * 评论状态： 0 未评价  1 已评价
     */
    private Integer commSts;
    /**
     * 推广员使用的推销卡号
     */
    private String distributionCardNo;
    /**
     * 加入购物车时间
     */
    private Date basketDate;


    public Long getOrderItemId() {
        return orderItemId;
    }

    public void setOrderItemId(Long orderItemId) {
        this.orderItemId = orderItemId;
    }

    public Long getShopId() {
        return shopId;
    }

    public void setShopId(Long shopId) {
        this.shopId = shopId;
    }

    public String getOrderNumber() {
        return orderNumber;
    }

    public void setOrderNumber(String orderNumber) {
        this.orderNumber = orderNumber;
    }

    public Long getProdId() {
        return prodId;
    }

    public void setProdId(Long prodId) {
        this.prodId = prodId;
    }

    public Long getSkuId() {
        return skuId;
    }

    public void setSkuId(Long skuId) {
        this.skuId = skuId;
    }

    public Integer getProdCount() {
        return prodCount;
    }

    public void setProdCount(Integer prodCount) {
        this.prodCount = prodCount;
    }

    public String getProdName() {
        return prodName;
    }

    public void setProdName(String prodName) {
        this.prodName = prodName;
    }

    public String getSkuName() {
        return skuName;
    }

    public void setSkuName(String skuName) {
        this.skuName = skuName;
    }

    public String getPic() {
        return pic;
    }

    public void setPic(String pic) {
        this.pic = pic;
    }

    public Double getPrice() {
        return price;
    }

    public void setPrice(Double price) {
        this.price = price;
    }

    public Long getMemberId() {
        return memberId;
    }

    public void setMemberId(Long memberId) {
        this.memberId = memberId;
    }

    public Double getProductTotalAmount() {
        return productTotalAmount;
    }

    public void setProductTotalAmount(Double productTotalAmount) {
        this.productTotalAmount = productTotalAmount;
    }

    public Date getRecTime() {
        return recTime;
    }

    public void setRecTime(Date recTime) {
        this.recTime = recTime;
    }

    public Integer getCommSts() {
        return commSts;
    }

    public void setCommSts(Integer commSts) {
        this.commSts = commSts;
    }

    public String getDistributionCardNo() {
        return distributionCardNo;
    }

    public void setDistributionCardNo(String distributionCardNo) {
        this.distributionCardNo = distributionCardNo;
    }

    public Date getBasketDate() {
        return basketDate;
    }

    public void setBasketDate(Date basketDate) {
        this.basketDate = basketDate;
    }

    @Override
    public String toString() {
        return "OrderItem{" +
                "orderItemId=" + orderItemId +
                ", shopId=" + shopId +
                ", orderNumber='" + orderNumber + '\'' +
                ", prodId=" + prodId +
                ", skuId=" + skuId +
                ", prodCount=" + prodCount +
                ", prodName='" + prodName + '\'' +
                ", skuName='" + skuName + '\'' +
                ", pic='" + pic + '\'' +
                ", price=" + price +
                ", memberId=" + memberId +
                ", productTotalAmount=" + productTotalAmount +
                ", recTime=" + recTime +
                ", commSts=" + commSts +
                ", distributionCardNo='" + distributionCardNo + '\'' +
                ", basketDate=" + basketDate +
                '}';
    }
}

