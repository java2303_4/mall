package com.ruoyi.order.domain;

public class OrderCount {
    private Integer allCount;
    private Integer close;
    private Integer confirm;
    private Integer consignment;
    private Integer payed;
    private Integer success;
    private Integer unPay;

    public Integer getAllCount() {
        return allCount;
    }

    public void setAllCount(Integer allCount) {
        this.allCount = allCount;
    }

    public Integer getClose() {
        return close;
    }

    public void setClose(Integer close) {
        this.close = close;
    }

    public Integer getConfirm() {
        return confirm;
    }

    public void setConfirm(Integer confirm) {
        this.confirm = confirm;
    }

    public Integer getConsignment() {
        return consignment;
    }

    public void setConsignment(Integer consignment) {
        this.consignment = consignment;
    }

    public Integer getPayed() {
        return payed;
    }

    public void setPayed(Integer payed) {
        this.payed = payed;
    }

    public Integer getSuccess() {
        return success;
    }

    public void setSuccess(Integer success) {
        this.success = success;
    }

    public Integer getUnPay() {
        return unPay;
    }

    public void setUnPay(Integer unPay) {
        this.unPay = unPay;
    }
}
