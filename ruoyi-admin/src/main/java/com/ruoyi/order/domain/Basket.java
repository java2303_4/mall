package com.ruoyi.order.domain;

import java.util.Date;
import com.fasterxml.jackson.annotation.JsonFormat;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.ruoyi.common.annotation.Excel;
import com.ruoyi.common.core.domain.BaseEntity;

/**
 * 购物车对象 tz_basket
 * 
 * @author ruoyi
 * @date 2024-01-14
 */
public class Basket extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** $column.columnComment */
    private Long basketId;

    /** $column.columnComment */
    @Excel(name = "${comment}", readConverterExp = "$column.readConverterExp()")
    private Long shopId;

    /** $column.columnComment */
    @Excel(name = "${comment}", readConverterExp = "$column.readConverterExp()")
    private Long prodId;

    /** $column.columnComment */
    @Excel(name = "${comment}", readConverterExp = "$column.readConverterExp()")
    private Long skuId;

    /** $column.columnComment */
    @Excel(name = "${comment}", readConverterExp = "$column.readConverterExp()")
    private String userId;

    /** $column.columnComment */
    @Excel(name = "${comment}", readConverterExp = "$column.readConverterExp()")
    private Long basketCount;

    /** $column.columnComment */
    @Excel(name = "${comment}", readConverterExp = "$column.readConverterExp()")
    private Date basketDate;

    /** $column.columnComment */
    @Excel(name = "${comment}", readConverterExp = "$column.readConverterExp()")
    private Long discountId;

    /** $column.columnComment */
    @Excel(name = "${comment}", readConverterExp = "$column.readConverterExp()")
    private String distributionCardNo;

    public void setBasketId(Long basketId) 
    {
        this.basketId = basketId;
    }

    public Long getBasketId() 
    {
        return basketId;
    }
    public void setShopId(Long shopId) 
    {
        this.shopId = shopId;
    }

    public Long getShopId() 
    {
        return shopId;
    }
    public void setProdId(Long prodId) 
    {
        this.prodId = prodId;
    }

    public Long getProdId() 
    {
        return prodId;
    }
    public void setSkuId(Long skuId) 
    {
        this.skuId = skuId;
    }

    public Long getSkuId() 
    {
        return skuId;
    }
    public void setUserId(String userId) 
    {
        this.userId = userId;
    }

    public String getUserId() 
    {
        return userId;
    }
    public void setBasketCount(Long basketCount) 
    {
        this.basketCount = basketCount;
    }

    public Long getBasketCount() 
    {
        return basketCount;
    }
    public void setBasketDate(Date basketDate) 
    {
        this.basketDate = basketDate;
    }

    public Date getBasketDate() 
    {
        return basketDate;
    }
    public void setDiscountId(Long discountId) 
    {
        this.discountId = discountId;
    }

    public Long getDiscountId() 
    {
        return discountId;
    }
    public void setDistributionCardNo(String distributionCardNo) 
    {
        this.distributionCardNo = distributionCardNo;
    }

    public String getDistributionCardNo() 
    {
        return distributionCardNo;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("basketId", getBasketId())
            .append("shopId", getShopId())
            .append("prodId", getProdId())
            .append("skuId", getSkuId())
            .append("userId", getUserId())
            .append("basketCount", getBasketCount())
            .append("basketDate", getBasketDate())
            .append("discountId", getDiscountId())
            .append("distributionCardNo", getDistributionCardNo())
            .toString();
    }
}
