package com.ruoyi.order.controller;

import java.util.List;
import javax.servlet.http.HttpServletResponse;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.order.domain.Area;
import com.ruoyi.order.service.IAreaService;
import com.ruoyi.common.utils.poi.ExcelUtil;

/**
 * 地址管理Controller
 * 
 * @author ruoyi
 * @date 2024-01-03
 */
@RestController
@RequestMapping("/order/area")
public class AreaController extends BaseController
{
    @Autowired
    private IAreaService areaService;

    /**
     * 查询地址管理列表
     */
    @PreAuthorize("@ss.hasPermi('order:area:list')")
    @GetMapping("/list")
    public AjaxResult list(Area area)
    {
        List<Area> list = areaService.selectAreaList(area);
        return success(list);
    }

    /**
     * 导出地址管理列表
     */
    @PreAuthorize("@ss.hasPermi('order:area:export')")
    @Log(title = "地址管理", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    public void export(HttpServletResponse response, Area area)
    {
        List<Area> list = areaService.selectAreaList(area);
        ExcelUtil<Area> util = new ExcelUtil<Area>(Area.class);
        util.exportExcel(response, list, "地址管理数据");
    }

    /**
     * 获取地址管理详细信息
     */
    @PreAuthorize("@ss.hasPermi('order:area:query')")
    @GetMapping(value = "/{areaId}")
    public AjaxResult getInfo(@PathVariable("areaId") Long areaId)
    {
        return success(areaService.selectAreaByAreaId(areaId));
    }

    /**
     * 新增地址管理
     */
    @PreAuthorize("@ss.hasPermi('order:area:add')")
    @Log(title = "地址管理", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody Area area)
    {
        return toAjax(areaService.insertArea(area));
    }

    /**
     * 修改地址管理
     */
    @PreAuthorize("@ss.hasPermi('order:area:edit')")
    @Log(title = "地址管理", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody Area area)
    {
        return toAjax(areaService.updateArea(area));
    }

    /**
     * 删除地址管理
     */
    @PreAuthorize("@ss.hasPermi('order:area:remove')")
    @Log(title = "地址管理", businessType = BusinessType.DELETE)
	@DeleteMapping("/{areaIds}")
    public AjaxResult remove(@PathVariable Long[] areaIds)
    {
        return toAjax(areaService.deleteAreaByAreaIds(areaIds));
    }
}
