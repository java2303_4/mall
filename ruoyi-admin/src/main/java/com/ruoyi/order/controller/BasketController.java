package com.ruoyi.order.controller;

import java.util.List;
import javax.servlet.http.HttpServletResponse;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.order.domain.Basket;
import com.ruoyi.order.service.IBasketService;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.common.core.page.TableDataInfo;

/**
 * 购物车Controller
 * 
 * @author ruoyi
 * @date 2024-01-14
 */
@RestController
@RequestMapping("/order/basket")
public class BasketController extends BaseController
{
    @Autowired
    private IBasketService basketService;

    /**
     * 查询购物车列表
     */
    @PreAuthorize("@ss.hasPermi('order:basket:list')")
    @GetMapping("/list")
    public TableDataInfo list(Basket basket)
    {
        startPage();
        List<Basket> list = basketService.selectBasketList(basket);
        return getDataTable(list);
    }

    /**
     * 导出购物车列表
     */
    @PreAuthorize("@ss.hasPermi('order:basket:export')")
    @Log(title = "购物车", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    public void export(HttpServletResponse response, Basket basket)
    {
        List<Basket> list = basketService.selectBasketList(basket);
        ExcelUtil<Basket> util = new ExcelUtil<Basket>(Basket.class);
        util.exportExcel(response, list, "购物车数据");
    }

    /**
     * 获取购物车详细信息
     */
    @PreAuthorize("@ss.hasPermi('order:basket:query')")
    @GetMapping(value = "/{basketId}")
    public AjaxResult getInfo(@PathVariable("basketId") Long basketId)
    {
        return success(basketService.selectBasketByBasketId(basketId));
    }

    /**
     * 新增购物车
     */
    @PreAuthorize("@ss.hasPermi('order:basket:add')")
    @Log(title = "购物车", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody Basket basket)
    {
        return toAjax(basketService.insertBasket(basket));
    }

    /**
     * 修改购物车
     */
    @PreAuthorize("@ss.hasPermi('order:basket:edit')")
    @Log(title = "购物车", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody Basket basket)
    {
        return toAjax(basketService.updateBasket(basket));
    }

    /**
     * 删除购物车
     */
    @PreAuthorize("@ss.hasPermi('order:basket:remove')")
    @Log(title = "购物车", businessType = BusinessType.DELETE)
	@DeleteMapping("/{basketIds}")
    public AjaxResult remove(@PathVariable Long[] basketIds)
    {
        return toAjax(basketService.deleteBasketByBasketIds(basketIds));
    }
}
