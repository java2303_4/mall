package com.ruoyi.order.controller;

import java.util.List;
import javax.servlet.http.HttpServletResponse;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.order.domain.UserAddr;
import com.ruoyi.order.service.IUserAddrService;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.common.core.page.TableDataInfo;

/**
 * 用户配送地址Controller
 * 
 * @author ruoyi
 * @date 2024-01-14
 */
@RestController
@RequestMapping("/order/addr")
public class UserAddrController extends BaseController
{
    @Autowired
    private IUserAddrService userAddrService;

    /**
     * 查询用户配送地址列表
     */
    @PreAuthorize("@ss.hasPermi('order:addr:list')")
    @GetMapping("/list")
    public TableDataInfo list(UserAddr userAddr)
    {
        startPage();
        List<UserAddr> list = userAddrService.selectUserAddrList(userAddr);
        return getDataTable(list);
    }

    /**
     * 导出用户配送地址列表
     */
    @PreAuthorize("@ss.hasPermi('order:addr:export')")
    @Log(title = "用户配送地址", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    public void export(HttpServletResponse response, UserAddr userAddr)
    {
        List<UserAddr> list = userAddrService.selectUserAddrList(userAddr);
        ExcelUtil<UserAddr> util = new ExcelUtil<UserAddr>(UserAddr.class);
        util.exportExcel(response, list, "用户配送地址数据");
    }

    /**
     * 获取用户配送地址详细信息
     */
    @PreAuthorize("@ss.hasPermi('order:addr:query')")
    @GetMapping(value = "/{addrId}")
    public AjaxResult getInfo(@PathVariable("addrId") Long addrId)
    {
        return success(userAddrService.selectUserAddrByAddrId(addrId));
    }

    /**
     * 新增用户配送地址
     */
    @PreAuthorize("@ss.hasPermi('order:addr:add')")
    @Log(title = "用户配送地址", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody UserAddr userAddr)
    {
        return toAjax(userAddrService.insertUserAddr(userAddr));
    }

    /**
     * 修改用户配送地址
     */
    @PreAuthorize("@ss.hasPermi('order:addr:edit')")
    @Log(title = "用户配送地址", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody UserAddr userAddr)
    {
        return toAjax(userAddrService.updateUserAddr(userAddr));
    }

    /**
     * 删除用户配送地址
     */
    @PreAuthorize("@ss.hasPermi('order:addr:remove')")
    @Log(title = "用户配送地址", businessType = BusinessType.DELETE)
	@DeleteMapping("/{addrIds}")
    public AjaxResult remove(@PathVariable Long[] addrIds)
    {
        return toAjax(userAddrService.deleteUserAddrByAddrIds(addrIds));
    }
}
