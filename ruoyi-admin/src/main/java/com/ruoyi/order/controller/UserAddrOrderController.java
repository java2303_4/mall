package com.ruoyi.order.controller;

import java.util.List;
import javax.servlet.http.HttpServletResponse;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.order.domain.UserAddrOrder;
import com.ruoyi.order.service.IUserAddrOrderService;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.common.core.page.TableDataInfo;

/**
 * 用户订单配送地址Controller
 * 
 * @author ruoyi
 * @date 2024-01-14
 */
@RestController
@RequestMapping("/order/order1")
public class UserAddrOrderController extends BaseController
{
    @Autowired
    private IUserAddrOrderService userAddrOrderService;

    /**
     * 查询用户订单配送地址列表
     */
    @PreAuthorize("@ss.hasPermi('order:order:list')")
    @GetMapping("/list")
    public TableDataInfo list(UserAddrOrder userAddrOrder)
    {
        startPage();
        List<UserAddrOrder> list = userAddrOrderService.selectUserAddrOrderList(userAddrOrder);
        return getDataTable(list);
    }

    /**
     * 导出用户订单配送地址列表
     */
    @PreAuthorize("@ss.hasPermi('order:order:export')")
    @Log(title = "用户订单配送地址", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    public void export(HttpServletResponse response, UserAddrOrder userAddrOrder)
    {
        List<UserAddrOrder> list = userAddrOrderService.selectUserAddrOrderList(userAddrOrder);
        ExcelUtil<UserAddrOrder> util = new ExcelUtil<UserAddrOrder>(UserAddrOrder.class);
        util.exportExcel(response, list, "用户订单配送地址数据");
    }

    /**
     * 获取用户订单配送地址详细信息
     */
    @PreAuthorize("@ss.hasPermi('order:order:query')")
    @GetMapping(value = "/{addrOrderId}")
    public AjaxResult getInfo(@PathVariable("addrOrderId") Long addrOrderId)
    {
        return success(userAddrOrderService.selectUserAddrOrderByAddrOrderId(addrOrderId));
    }

    /**
     * 新增用户订单配送地址
     */
    @PreAuthorize("@ss.hasPermi('order:order:add')")
    @Log(title = "用户订单配送地址", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody UserAddrOrder userAddrOrder)
    {
        return toAjax(userAddrOrderService.insertUserAddrOrder(userAddrOrder));
    }

    /**
     * 修改用户订单配送地址
     */
    @PreAuthorize("@ss.hasPermi('order:order:edit')")
    @Log(title = "用户订单配送地址", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody UserAddrOrder userAddrOrder)
    {
        return toAjax(userAddrOrderService.updateUserAddrOrder(userAddrOrder));
    }

    /**
     * 删除用户订单配送地址
     */
    @PreAuthorize("@ss.hasPermi('order:order:remove')")
    @Log(title = "用户订单配送地址", businessType = BusinessType.DELETE)
	@DeleteMapping("/{addrOrderIds}")
    public AjaxResult remove(@PathVariable Long[] addrOrderIds)
    {
        return toAjax(userAddrOrderService.deleteUserAddrOrderByAddrOrderIds(addrOrderIds));
    }
}
