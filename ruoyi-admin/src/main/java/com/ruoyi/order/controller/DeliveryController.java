package com.ruoyi.order.controller;

import java.util.List;
import javax.servlet.http.HttpServletResponse;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.order.domain.Delivery;
import com.ruoyi.order.service.IDeliveryService;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.common.core.page.TableDataInfo;

/**
 * 物流公司Controller
 * 
 * @author ruoyi
 * @date 2024-01-15
 */
@RestController
@RequestMapping("/order/delivery")
public class DeliveryController extends BaseController
{
    @Autowired
    private IDeliveryService deliveryService;

    /**
     * 查询物流公司列表
     */
    @PreAuthorize("@ss.hasPermi('order:delivery:list')")
    @GetMapping("/list")
    public TableDataInfo list(Delivery delivery)
    {
        startPage();
        List<Delivery> list = deliveryService.selectDeliveryList(delivery);
        return getDataTable(list);
    }

    /**
     * 导出物流公司列表
     */
    @PreAuthorize("@ss.hasPermi('order:delivery:export')")
    @Log(title = "物流公司", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    public void export(HttpServletResponse response, Delivery delivery)
    {
        List<Delivery> list = deliveryService.selectDeliveryList(delivery);
        ExcelUtil<Delivery> util = new ExcelUtil<Delivery>(Delivery.class);
        util.exportExcel(response, list, "物流公司数据");
    }

    /**
     * 获取物流公司详细信息
     */
    @PreAuthorize("@ss.hasPermi('order:delivery:query')")
    @GetMapping(value = "/{dvyId}")
    public AjaxResult getInfo(@PathVariable("dvyId") Long dvyId)
    {
        return success(deliveryService.selectDeliveryByDvyId(dvyId));
    }

    /**
     * 新增物流公司
     */
    @PreAuthorize("@ss.hasPermi('order:delivery:add')")
    @Log(title = "物流公司", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody Delivery delivery)
    {
        return toAjax(deliveryService.insertDelivery(delivery));
    }

    /**
     * 修改物流公司
     */
    @PreAuthorize("@ss.hasPermi('order:delivery:edit')")
    @Log(title = "物流公司", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody Delivery delivery)
    {
        return toAjax(deliveryService.updateDelivery(delivery));
    }

    /**
     * 删除物流公司
     */
    @PreAuthorize("@ss.hasPermi('order:delivery:remove')")
    @Log(title = "物流公司", businessType = BusinessType.DELETE)
	@DeleteMapping("/{dvyIds}")
    public AjaxResult remove(@PathVariable Long[] dvyIds)
    {
        return toAjax(deliveryService.deleteDeliveryByDvyIds(dvyIds));
    }
}
