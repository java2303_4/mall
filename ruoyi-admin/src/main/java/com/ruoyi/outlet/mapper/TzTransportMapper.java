package com.ruoyi.outlet.mapper;

import java.util.List;
import com.ruoyi.outlet.domain.TzTransport;

/**
 * 运费模板Mapper接口
 * 
 * @author ruoyi
 * @date 2024-01-07
 */
public interface TzTransportMapper 
{
    /**
     * 查询运费模板
     * 
     * @param transportId 运费模板主键
     * @return 运费模板
     */
    public TzTransport selectTzTransportByTransportId(Long transportId);

    /**
     * 查询运费模板列表
     * 
     * @param tzTransport 运费模板
     * @return 运费模板集合
     */
    public List<TzTransport> selectTzTransportList(TzTransport tzTransport);

    /**
     * 新增运费模板
     * 
     * @param tzTransport 运费模板
     * @return 结果
     */
    public int insertTzTransport(TzTransport tzTransport);

    /**
     * 修改运费模板
     * 
     * @param tzTransport 运费模板
     * @return 结果
     */
    public int updateTzTransport(TzTransport tzTransport);

    /**
     * 删除运费模板
     * 
     * @param transportId 运费模板主键
     * @return 结果
     */
    public int deleteTzTransportByTransportId(Long transportId);

    /**
     * 批量删除运费模板
     * 
     * @param transportIds 需要删除的数据主键集合
     * @return 结果
     */
    public int deleteTzTransportByTransportIds(Long[] transportIds);
}
