package com.ruoyi.outlet.mapper;

import java.util.List;
import com.ruoyi.outlet.domain.TzHotSearch;

/**
 * 热搜管理Mapper接口
 * 
 * @author ruoyi
 * @date 2024-01-03
 */
public interface TzHotSearchMapper 
{
    /**
     * 查询热搜管理
     * 
     * @param hotSearchId 热搜管理主键
     * @return 热搜管理
     */
    public TzHotSearch selectTzHotSearchByHotSearchId(Long hotSearchId);

    /**
     * 查询热搜管理列表
     * 
     * @param tzHotSearch 热搜管理
     * @return 热搜管理集合
     */
    public List<TzHotSearch> selectTzHotSearchList(TzHotSearch tzHotSearch);

    /**
     * 新增热搜管理
     * 
     * @param tzHotSearch 热搜管理
     * @return 结果
     */
    public int insertTzHotSearch(TzHotSearch tzHotSearch);

    /**
     * 修改热搜管理
     * 
     * @param tzHotSearch 热搜管理
     * @return 结果
     */
    public int updateTzHotSearch(TzHotSearch tzHotSearch);

    /**
     * 删除热搜管理
     * 
     * @param hotSearchId 热搜管理主键
     * @return 结果
     */
    public int deleteTzHotSearchByHotSearchId(Long hotSearchId);

    /**
     * 批量删除热搜管理
     * 
     * @param hotSearchIds 需要删除的数据主键集合
     * @return 结果
     */
    public int deleteTzHotSearchByHotSearchIds(Long[] hotSearchIds);
}
