package com.ruoyi.outlet.service;

import java.util.List;
import com.ruoyi.outlet.domain.TzShopDetail;

/**
 * 店铺Service接口
 * 
 * @author ruoyi
 * @date 2024-01-07
 */
public interface ITzShopDetailService 
{
    /**
     * 查询店铺
     * 
     * @param shopId 店铺主键
     * @return 店铺
     */
    public TzShopDetail selectTzShopDetailByShopId(Long shopId);

    /**
     * 查询店铺列表
     * 
     * @param tzShopDetail 店铺
     * @return 店铺集合
     */
    public List<TzShopDetail> selectTzShopDetailList(TzShopDetail tzShopDetail);

    /**
     * 新增店铺
     * 
     * @param tzShopDetail 店铺
     * @return 结果
     */
    public int insertTzShopDetail(TzShopDetail tzShopDetail);

    /**
     * 修改店铺
     * 
     * @param tzShopDetail 店铺
     * @return 结果
     */
    public int updateTzShopDetail(TzShopDetail tzShopDetail);

    /**
     * 批量删除店铺
     * 
     * @param shopIds 需要删除的店铺主键集合
     * @return 结果
     */
    public int deleteTzShopDetailByShopIds(Long[] shopIds);

    /**
     * 删除店铺信息
     * 
     * @param shopId 店铺主键
     * @return 结果
     */
    public int deleteTzShopDetailByShopId(Long shopId);
}
