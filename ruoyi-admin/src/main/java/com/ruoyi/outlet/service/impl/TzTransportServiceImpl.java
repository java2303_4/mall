package com.ruoyi.outlet.service.impl;

import java.util.List;
import com.ruoyi.common.utils.DateUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ruoyi.outlet.mapper.TzTransportMapper;
import com.ruoyi.outlet.domain.TzTransport;
import com.ruoyi.outlet.service.ITzTransportService;

/**
 * 运费模板Service业务层处理
 * 
 * @author ruoyi
 * @date 2024-01-07
 */
@Service
public class TzTransportServiceImpl implements ITzTransportService 
{
    @Autowired
    private TzTransportMapper tzTransportMapper;

    /**
     * 查询运费模板
     * 
     * @param transportId 运费模板主键
     * @return 运费模板
     */
    @Override
    public TzTransport selectTzTransportByTransportId(Long transportId)
    {
        return tzTransportMapper.selectTzTransportByTransportId(transportId);
    }

    /**
     * 查询运费模板列表
     * 
     * @param tzTransport 运费模板
     * @return 运费模板
     */
    @Override
    public List<TzTransport> selectTzTransportList(TzTransport tzTransport)
    {
        return tzTransportMapper.selectTzTransportList(tzTransport);
    }

    /**
     * 新增运费模板
     * 
     * @param tzTransport 运费模板
     * @return 结果
     */
    @Override
    public int insertTzTransport(TzTransport tzTransport)
    {
        tzTransport.setCreateTime(DateUtils.getNowDate());
        return tzTransportMapper.insertTzTransport(tzTransport);
    }

    /**
     * 修改运费模板
     * 
     * @param tzTransport 运费模板
     * @return 结果
     */
    @Override
    public int updateTzTransport(TzTransport tzTransport)
    {
        return tzTransportMapper.updateTzTransport(tzTransport);
    }

    /**
     * 批量删除运费模板
     * 
     * @param transportIds 需要删除的运费模板主键
     * @return 结果
     */
    @Override
    public int deleteTzTransportByTransportIds(Long[] transportIds)
    {
        return tzTransportMapper.deleteTzTransportByTransportIds(transportIds);
    }

    /**
     * 删除运费模板信息
     * 
     * @param transportId 运费模板主键
     * @return 结果
     */
    @Override
    public int deleteTzTransportByTransportId(Long transportId)
    {
        return tzTransportMapper.deleteTzTransportByTransportId(transportId);
    }
}
