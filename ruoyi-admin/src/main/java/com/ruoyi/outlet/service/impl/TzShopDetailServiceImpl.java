package com.ruoyi.outlet.service.impl;

import java.util.List;
import com.ruoyi.common.utils.DateUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ruoyi.outlet.mapper.TzShopDetailMapper;
import com.ruoyi.outlet.domain.TzShopDetail;
import com.ruoyi.outlet.service.ITzShopDetailService;

/**
 * 店铺Service业务层处理
 * 
 * @author ruoyi
 * @date 2024-01-07
 */
@Service
public class TzShopDetailServiceImpl implements ITzShopDetailService 
{
    @Autowired
    private TzShopDetailMapper tzShopDetailMapper;

    /**
     * 查询店铺
     * 
     * @param shopId 店铺主键
     * @return 店铺
     */
    @Override
    public TzShopDetail selectTzShopDetailByShopId(Long shopId)
    {
        return tzShopDetailMapper.selectTzShopDetailByShopId(shopId);
    }

    /**
     * 查询店铺列表
     * 
     * @param tzShopDetail 店铺
     * @return 店铺
     */
    @Override
    public List<TzShopDetail> selectTzShopDetailList(TzShopDetail tzShopDetail)
    {
        return tzShopDetailMapper.selectTzShopDetailList(tzShopDetail);
    }

    /**
     * 新增店铺
     * 
     * @param tzShopDetail 店铺
     * @return 结果
     */
    @Override
    public int insertTzShopDetail(TzShopDetail tzShopDetail)
    {
        tzShopDetail.setCreateTime(DateUtils.getNowDate());
        return tzShopDetailMapper.insertTzShopDetail(tzShopDetail);
    }

    /**
     * 修改店铺
     * 
     * @param tzShopDetail 店铺
     * @return 结果
     */
    @Override
    public int updateTzShopDetail(TzShopDetail tzShopDetail)
    {
        tzShopDetail.setUpdateTime(DateUtils.getNowDate());
        return tzShopDetailMapper.updateTzShopDetail(tzShopDetail);
    }

    /**
     * 批量删除店铺
     * 
     * @param shopIds 需要删除的店铺主键
     * @return 结果
     */
    @Override
    public int deleteTzShopDetailByShopIds(Long[] shopIds)
    {
        return tzShopDetailMapper.deleteTzShopDetailByShopIds(shopIds);
    }

    /**
     * 删除店铺信息
     * 
     * @param shopId 店铺主键
     * @return 结果
     */
    @Override
    public int deleteTzShopDetailByShopId(Long shopId)
    {
        return tzShopDetailMapper.deleteTzShopDetailByShopId(shopId);
    }
}
