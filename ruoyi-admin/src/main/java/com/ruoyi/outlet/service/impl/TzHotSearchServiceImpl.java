package com.ruoyi.outlet.service.impl;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ruoyi.outlet.mapper.TzHotSearchMapper;
import com.ruoyi.outlet.domain.TzHotSearch;
import com.ruoyi.outlet.service.ITzHotSearchService;

/**
 * 热搜管理Service业务层处理
 * 
 * @author ruoyi
 * @date 2024-01-03
 */
@Service
public class TzHotSearchServiceImpl implements ITzHotSearchService 
{
    @Autowired
    private TzHotSearchMapper tzHotSearchMapper;

    /**
     * 查询热搜管理
     * 
     * @param hotSearchId 热搜管理主键
     * @return 热搜管理
     */
    @Override
    public TzHotSearch selectTzHotSearchByHotSearchId(Long hotSearchId)
    {
        return tzHotSearchMapper.selectTzHotSearchByHotSearchId(hotSearchId);
    }

    /**
     * 查询热搜管理列表
     * 
     * @param tzHotSearch 热搜管理
     * @return 热搜管理
     */
    @Override
    public List<TzHotSearch> selectTzHotSearchList(TzHotSearch tzHotSearch)
    {
        return tzHotSearchMapper.selectTzHotSearchList(tzHotSearch);
    }

    /**
     * 新增热搜管理
     * 
     * @param tzHotSearch 热搜管理
     * @return 结果
     */
    @Override
    public int insertTzHotSearch(TzHotSearch tzHotSearch)
    {
        return tzHotSearchMapper.insertTzHotSearch(tzHotSearch);
    }

    /**
     * 修改热搜管理
     * 
     * @param tzHotSearch 热搜管理
     * @return 结果
     */
    @Override
    public int updateTzHotSearch(TzHotSearch tzHotSearch)
    {
        return tzHotSearchMapper.updateTzHotSearch(tzHotSearch);
    }

    /**
     * 批量删除热搜管理
     * 
     * @param hotSearchIds 需要删除的热搜管理主键
     * @return 结果
     */
    @Override
    public int deleteTzHotSearchByHotSearchIds(Long[] hotSearchIds)
    {
        return tzHotSearchMapper.deleteTzHotSearchByHotSearchIds(hotSearchIds);
    }

    /**
     * 删除热搜管理信息
     * 
     * @param hotSearchId 热搜管理主键
     * @return 结果
     */
    @Override
    public int deleteTzHotSearchByHotSearchId(Long hotSearchId)
    {
        return tzHotSearchMapper.deleteTzHotSearchByHotSearchId(hotSearchId);
    }
}
