package com.ruoyi.outlet.domain;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.ruoyi.common.annotation.Excel;
import com.ruoyi.common.core.domain.BaseEntity;

/**
 * 运费模板对象 tz_transport
 * 
 * @author ruoyi
 * @date 2024-01-07
 */
public class TzTransport extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** 运费模板ID */
    private Long transportId;

    /** 模板名称 */
    @Excel(name = "模板名称")
    private String transName;

    /** 店铺Id */
    private Long shopId;

    /** 收费方式 */
    private Integer chargeType;

    /** 是否包邮 */
    private Integer isFreeFee;

    /** 是否含有包邮条件 */
    private Integer hasFreeCondition;

    /** 模板类型 */
    private String type;

    public void setTransportId(Long transportId) 
    {
        this.transportId = transportId;
    }

    public Long getTransportId() 
    {
        return transportId;
    }
    public void setTransName(String transName) 
    {
        this.transName = transName;
    }

    public String getTransName() 
    {
        return transName;
    }
    public void setShopId(Long shopId) 
    {
        this.shopId = shopId;
    }

    public Long getShopId() 
    {
        return shopId;
    }
    public void setChargeType(Integer chargeType) 
    {
        this.chargeType = chargeType;
    }

    public Integer getChargeType() 
    {
        return chargeType;
    }
    public void setIsFreeFee(Integer isFreeFee) 
    {
        this.isFreeFee = isFreeFee;
    }

    public Integer getIsFreeFee() 
    {
        return isFreeFee;
    }
    public void setHasFreeCondition(Integer hasFreeCondition) 
    {
        this.hasFreeCondition = hasFreeCondition;
    }

    public Integer getHasFreeCondition() 
    {
        return hasFreeCondition;
    }
    public void setType(String type) 
    {
        this.type = type;
    }

    public String getType() 
    {
        return type;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("transportId", getTransportId())
            .append("transName", getTransName())
            .append("createTime", getCreateTime())
            .append("shopId", getShopId())
            .append("chargeType", getChargeType())
            .append("isFreeFee", getIsFreeFee())
            .append("hasFreeCondition", getHasFreeCondition())
            .append("type", getType())
            .toString();
    }
}
