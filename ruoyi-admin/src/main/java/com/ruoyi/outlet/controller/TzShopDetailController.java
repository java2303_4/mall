package com.ruoyi.outlet.controller;

import java.util.List;
import javax.servlet.http.HttpServletResponse;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.outlet.domain.TzShopDetail;
import com.ruoyi.outlet.service.ITzShopDetailService;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.common.core.page.TableDataInfo;

/**
 * 店铺Controller
 * 
 * @author ruoyi
 * @date 2024-01-07
 */
@RestController
@RequestMapping("/outlet/detail")
public class TzShopDetailController extends BaseController
{
    @Autowired
    private ITzShopDetailService tzShopDetailService;

    /**
     * 查询店铺列表
     */
    @PreAuthorize("@ss.hasPermi('outlet:detail:list')")
    @GetMapping("/list")
    public TableDataInfo list(TzShopDetail tzShopDetail)
    {
        startPage();
        List<TzShopDetail> list = tzShopDetailService.selectTzShopDetailList(tzShopDetail);
        return getDataTable(list);
    }

    /**
     * 导出店铺列表
     */
    @PreAuthorize("@ss.hasPermi('outlet:detail:export')")
    @Log(title = "店铺", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    public void export(HttpServletResponse response, TzShopDetail tzShopDetail)
    {
        List<TzShopDetail> list = tzShopDetailService.selectTzShopDetailList(tzShopDetail);
        ExcelUtil<TzShopDetail> util = new ExcelUtil<TzShopDetail>(TzShopDetail.class);
        util.exportExcel(response, list, "店铺数据");
    }

    /**
     * 获取店铺详细信息
     */
    @PreAuthorize("@ss.hasPermi('outlet:detail:query')")
    @GetMapping(value = "/{shopId}")
    public AjaxResult getInfo(@PathVariable("shopId") Long shopId)
    {
        return success(tzShopDetailService.selectTzShopDetailByShopId(shopId));
    }

    /**
     * 新增店铺
     */
    @PreAuthorize("@ss.hasPermi('outlet:detail:add')")
    @Log(title = "店铺", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody TzShopDetail tzShopDetail)
    {
        return toAjax(tzShopDetailService.insertTzShopDetail(tzShopDetail));
    }

    /**
     * 修改店铺
     */
    @PreAuthorize("@ss.hasPermi('outlet:detail:edit')")
    @Log(title = "店铺", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody TzShopDetail tzShopDetail)
    {
        return toAjax(tzShopDetailService.updateTzShopDetail(tzShopDetail));
    }

    /**
     * 删除店铺
     */
    @PreAuthorize("@ss.hasPermi('outlet:detail:remove')")
    @Log(title = "店铺", businessType = BusinessType.DELETE)
	@DeleteMapping("/{shopIds}")
    public AjaxResult remove(@PathVariable Long[] shopIds)
    {
        return toAjax(tzShopDetailService.deleteTzShopDetailByShopIds(shopIds));
    }
}
