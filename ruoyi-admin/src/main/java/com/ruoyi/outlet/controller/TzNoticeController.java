package com.ruoyi.outlet.controller;

import java.util.List;
import javax.servlet.http.HttpServletResponse;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.outlet.domain.TzNotice;
import com.ruoyi.outlet.service.ITzNoticeService;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.common.core.page.TableDataInfo;

/**
 * 公告Controller
 * 
 * @author ruoyi
 * @date 2024-01-03
 */
@RestController
@RequestMapping("/outlet/notice")
public class TzNoticeController extends BaseController
{
    @Autowired
    private ITzNoticeService tzNoticeService;

    /**
     * 查询公告列表
     */
    @PreAuthorize("@ss.hasPermi('outlet:notice:list')")
    @GetMapping("/list")
    public TableDataInfo list(TzNotice tzNotice)
    {
        startPage();
        List<TzNotice> list = tzNoticeService.selectTzNoticeList(tzNotice);
        return getDataTable(list);
    }

    /**
     * 导出公告列表
     */
    @PreAuthorize("@ss.hasPermi('outlet:notice:export')")
    @Log(title = "公告", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    public void export(HttpServletResponse response, TzNotice tzNotice)
    {
        List<TzNotice> list = tzNoticeService.selectTzNoticeList(tzNotice);
        ExcelUtil<TzNotice> util = new ExcelUtil<TzNotice>(TzNotice.class);
        util.exportExcel(response, list, "公告数据");
    }

    /**
     * 获取公告详细信息
     */
    @PreAuthorize("@ss.hasPermi('outlet:notice:query')")
    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@PathVariable("id") Long id)
    {
        return success(tzNoticeService.selectTzNoticeById(id));
    }

    /**
     * 新增公告
     */
    @PreAuthorize("@ss.hasPermi('outlet:notice:add')")
    @Log(title = "公告", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody TzNotice tzNotice)
    {
        return toAjax(tzNoticeService.insertTzNotice(tzNotice));
    }

    /**
     * 修改公告
     */
    @PreAuthorize("@ss.hasPermi('outlet:notice:edit')")
    @Log(title = "公告", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody TzNotice tzNotice)
    {
        return toAjax(tzNoticeService.updateTzNotice(tzNotice));
    }

    /**
     * 删除公告
     */
    @PreAuthorize("@ss.hasPermi('outlet:notice:remove')")
    @Log(title = "公告", businessType = BusinessType.DELETE)
	@DeleteMapping("/{ids}")
    public AjaxResult remove(@PathVariable Long[] ids)
    {
        return toAjax(tzNoticeService.deleteTzNoticeByIds(ids));
    }
}
