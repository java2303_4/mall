package com.ruoyi.outlet.controller;

import java.util.List;
import javax.servlet.http.HttpServletResponse;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.outlet.domain.TzTransport;
import com.ruoyi.outlet.service.ITzTransportService;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.common.core.page.TableDataInfo;

/**
 * 运费模板Controller
 * 
 * @author ruoyi
 * @date 2024-01-07
 */
@RestController
@RequestMapping("/outlet/transport")
public class TzTransportController extends BaseController
{
    @Autowired
    private ITzTransportService tzTransportService;

    /**
     * 查询运费模板列表
     */
    @PreAuthorize("@ss.hasPermi('outlet:transport:list')")
    @GetMapping("/list")
    public TableDataInfo list(TzTransport tzTransport)
    {
        startPage();
        List<TzTransport> list = tzTransportService.selectTzTransportList(tzTransport);
        return getDataTable(list);
    }

    /**
     * 导出运费模板列表
     */
    @PreAuthorize("@ss.hasPermi('outlet:transport:export')")
    @Log(title = "运费模板", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    public void export(HttpServletResponse response, TzTransport tzTransport)
    {
        List<TzTransport> list = tzTransportService.selectTzTransportList(tzTransport);
        ExcelUtil<TzTransport> util = new ExcelUtil<TzTransport>(TzTransport.class);
        util.exportExcel(response, list, "运费模板数据");
    }

    /**
     * 获取运费模板详细信息
     */
    @PreAuthorize("@ss.hasPermi('outlet:transport:query')")
    @GetMapping(value = "/{transportId}")
    public AjaxResult getInfo(@PathVariable("transportId") Long transportId)
    {
        return success(tzTransportService.selectTzTransportByTransportId(transportId));
    }

    /**
     * 新增运费模板
     */
    @PreAuthorize("@ss.hasPermi('outlet:transport:add')")
    @Log(title = "运费模板", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody TzTransport tzTransport)
    {
        return toAjax(tzTransportService.insertTzTransport(tzTransport));
    }

    /**
     * 修改运费模板
     */
    @PreAuthorize("@ss.hasPermi('outlet:transport:edit')")
    @Log(title = "运费模板", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody TzTransport tzTransport)
    {
        return toAjax(tzTransportService.updateTzTransport(tzTransport));
    }

    /**
     * 删除运费模板
     */
    @PreAuthorize("@ss.hasPermi('outlet:transport:remove')")
    @Log(title = "运费模板", businessType = BusinessType.DELETE)
	@DeleteMapping("/{transportIds}")
    public AjaxResult remove(@PathVariable Long[] transportIds)
    {
        return toAjax(tzTransportService.deleteTzTransportByTransportIds(transportIds));
    }
}
