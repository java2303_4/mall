package com.ruoyi.outlet.controller;

import java.util.List;
import javax.servlet.http.HttpServletResponse;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.outlet.domain.TzIndexImg;
import com.ruoyi.outlet.service.ITzIndexImgService;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.common.core.page.TableDataInfo;

/**
 * 主页轮播图Controller
 * 
 * @author ruoyi
 * @date 2024-01-04
 */
@RestController
@RequestMapping("/outlet/img")
public class TzIndexImgController extends BaseController
{
    @Autowired
    private ITzIndexImgService tzIndexImgService;

    /**
     * 查询主页轮播图列表
     */
    @PreAuthorize("@ss.hasPermi('outlet:img:list')")
    @GetMapping("/list")
    public TableDataInfo list(TzIndexImg tzIndexImg)
    {
        startPage();
        List<TzIndexImg> list = tzIndexImgService.selectTzIndexImgList(tzIndexImg);
        return getDataTable(list);
    }

    /**
     * 导出主页轮播图列表
     */
    @PreAuthorize("@ss.hasPermi('outlet:img:export')")
    @Log(title = "主页轮播图", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    public void export(HttpServletResponse response, TzIndexImg tzIndexImg)
    {
        List<TzIndexImg> list = tzIndexImgService.selectTzIndexImgList(tzIndexImg);
        ExcelUtil<TzIndexImg> util = new ExcelUtil<TzIndexImg>(TzIndexImg.class);
        util.exportExcel(response, list, "主页轮播图数据");
    }

    /**
     * 获取主页轮播图详细信息
     */
    @PreAuthorize("@ss.hasPermi('outlet:img:query')")
    @GetMapping(value = "/{imgId}")
    public AjaxResult getInfo(@PathVariable("imgId") Long imgId)
    {
        return success(tzIndexImgService.selectTzIndexImgByImgId(imgId));
    }

    /**
     * 新增主页轮播图
     */
    @PreAuthorize("@ss.hasPermi('outlet:img:add')")
    @Log(title = "主页轮播图", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody TzIndexImg tzIndexImg)
    {
        return toAjax(tzIndexImgService.insertTzIndexImg(tzIndexImg));
    }

    /**
     * 修改主页轮播图
     */
    @PreAuthorize("@ss.hasPermi('outlet:img:edit')")
    @Log(title = "主页轮播图", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody TzIndexImg tzIndexImg)
    {
        return toAjax(tzIndexImgService.updateTzIndexImg(tzIndexImg));
    }

    /**
     * 删除主页轮播图
     */
    @PreAuthorize("@ss.hasPermi('outlet:img:remove')")
    @Log(title = "主页轮播图", businessType = BusinessType.DELETE)
	@DeleteMapping("/{imgIds}")
    public AjaxResult remove(@PathVariable Long[] imgIds)
    {
        return toAjax(tzIndexImgService.deleteTzIndexImgByImgIds(imgIds));
    }
}
