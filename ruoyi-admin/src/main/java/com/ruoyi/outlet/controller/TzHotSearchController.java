package com.ruoyi.outlet.controller;

import java.util.List;
import javax.servlet.http.HttpServletResponse;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.outlet.domain.TzHotSearch;
import com.ruoyi.outlet.service.ITzHotSearchService;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.common.core.page.TableDataInfo;

/**
 * 热搜管理Controller
 * 
 * @author ruoyi
 * @date 2024-01-03
 */
@RestController
@RequestMapping("/outlet/search")
public class TzHotSearchController extends BaseController
{
    @Autowired
    private ITzHotSearchService tzHotSearchService;

    /**
     * 查询热搜管理列表
     */
    @PreAuthorize("@ss.hasPermi('outlet:search:list')")
    @GetMapping("/list")
    public TableDataInfo list(TzHotSearch tzHotSearch)
    {
        startPage();
        List<TzHotSearch> list = tzHotSearchService.selectTzHotSearchList(tzHotSearch);
        return getDataTable(list);
    }

    /**
     * 导出热搜管理列表
     */
    @PreAuthorize("@ss.hasPermi('outlet:search:export')")
    @Log(title = "热搜管理", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    public void export(HttpServletResponse response, TzHotSearch tzHotSearch)
    {
        List<TzHotSearch> list = tzHotSearchService.selectTzHotSearchList(tzHotSearch);
        ExcelUtil<TzHotSearch> util = new ExcelUtil<TzHotSearch>(TzHotSearch.class);
        util.exportExcel(response, list, "热搜管理数据");
    }

    /**
     * 获取热搜管理详细信息
     */
    @PreAuthorize("@ss.hasPermi('outlet:search:query')")
    @GetMapping(value = "/{hotSearchId}")
    public AjaxResult getInfo(@PathVariable("hotSearchId") Long hotSearchId)
    {
        return success(tzHotSearchService.selectTzHotSearchByHotSearchId(hotSearchId));
    }

    /**
     * 新增热搜管理
     */
    @PreAuthorize("@ss.hasPermi('outlet:search:add')")
    @Log(title = "热搜管理", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody TzHotSearch tzHotSearch)
    {
        return toAjax(tzHotSearchService.insertTzHotSearch(tzHotSearch));
    }

    /**
     * 修改热搜管理
     */
    @PreAuthorize("@ss.hasPermi('outlet:search:edit')")
    @Log(title = "热搜管理", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody TzHotSearch tzHotSearch)
    {
        return toAjax(tzHotSearchService.updateTzHotSearch(tzHotSearch));
    }

    /**
     * 删除热搜管理
     */
    @PreAuthorize("@ss.hasPermi('outlet:search:remove')")
    @Log(title = "热搜管理", businessType = BusinessType.DELETE)
	@DeleteMapping("/{hotSearchIds}")
    public AjaxResult remove(@PathVariable Long[] hotSearchIds)
    {
        return toAjax(tzHotSearchService.deleteTzHotSearchByHotSearchIds(hotSearchIds));
    }
}
