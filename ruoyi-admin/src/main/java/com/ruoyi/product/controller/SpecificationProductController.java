package com.ruoyi.product.controller;

import java.util.List;
import javax.servlet.http.HttpServletResponse;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.product.domain.SpecificationProduct;
import com.ruoyi.product.service.ISpecificationProductService;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.common.core.page.TableDataInfo;

/**
 * 规格属性中间表Controller
 * 
 * @author ruoyi
 * @date 2024-01-07
 */
@RestController
@RequestMapping("/product/specificationProduct")
public class SpecificationProductController extends BaseController
{
    @Autowired
    private ISpecificationProductService specificationProductService;

    /**
     * 查询规格属性中间表列表
     */
    @PreAuthorize("@ss.hasPermi('product:specificationProduct:list')")
    @GetMapping("/list")
    public TableDataInfo list(SpecificationProduct specificationProduct)
    {
        startPage();
        List<SpecificationProduct> list = specificationProductService.selectSpecificationProductList(specificationProduct);
        return getDataTable(list);
    }

    /**
     * 导出规格属性中间表列表
     */
    @PreAuthorize("@ss.hasPermi('product:specificationProduct:export')")
    @Log(title = "规格属性中间表", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    public void export(HttpServletResponse response, SpecificationProduct specificationProduct)
    {
        List<SpecificationProduct> list = specificationProductService.selectSpecificationProductList(specificationProduct);
        ExcelUtil<SpecificationProduct> util = new ExcelUtil<SpecificationProduct>(SpecificationProduct.class);
        util.exportExcel(response, list, "规格属性中间表数据");
    }

    /**
     * 获取规格属性中间表详细信息
     */
    @PreAuthorize("@ss.hasPermi('product:specificationProduct:query')")
    @GetMapping(value = "/{valueId}")
    public AjaxResult getInfo(@PathVariable("valueId") Long valueId)
    {
        return success(specificationProductService.selectSpecificationProductByValueId(valueId));
    }

    /**
     * 新增规格属性中间表
     */
    @PreAuthorize("@ss.hasPermi('product:specificationProduct:add')")
    @Log(title = "规格属性中间表", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody SpecificationProduct specificationProduct)
    {
        return toAjax(specificationProductService.insertSpecificationProduct(specificationProduct));
    }

    /**
     * 修改规格属性中间表
     */
    @PreAuthorize("@ss.hasPermi('product:specificationProduct:edit')")
    @Log(title = "规格属性中间表", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody SpecificationProduct specificationProduct)
    {
        return toAjax(specificationProductService.updateSpecificationProduct(specificationProduct));
    }

    /**
     * 删除规格属性中间表
     */
    @PreAuthorize("@ss.hasPermi('product:specificationProduct:remove')")
    @Log(title = "规格属性中间表", businessType = BusinessType.DELETE)
	@DeleteMapping("/{valueIds}")
    public AjaxResult remove(@PathVariable Long[] valueIds)
    {
        return toAjax(specificationProductService.deleteSpecificationProductByValueIds(valueIds));
    }
}
