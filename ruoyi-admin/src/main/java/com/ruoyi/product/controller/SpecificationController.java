package com.ruoyi.product.controller;

import java.util.List;
import javax.servlet.http.HttpServletResponse;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.product.domain.Specification;
import com.ruoyi.product.service.ISpecificationService;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.common.core.page.TableDataInfo;

/**
 * 规格管理Controller
 * 
 * @author ruoyi
 * @date 2024-01-03
 */
@RestController
@RequestMapping("/product/specification")
public class SpecificationController extends BaseController
{
    @Autowired
    private ISpecificationService specificationService;

    /**
     * 查询规格管理列表
     */
    @PreAuthorize("@ss.hasPermi('product:specification:list')")
    @GetMapping("/list")
    public TableDataInfo list(Specification specification)
    {
        startPage();
        List<Specification> list = specificationService.selectSpecificationList(specification);
        return getDataTable(list);
    }

    /**
     * 导出规格管理列表
     */
    @PreAuthorize("@ss.hasPermi('product:specification:export')")
    @Log(title = "规格管理", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    public void export(HttpServletResponse response, Specification specification)
    {
        List<Specification> list = specificationService.selectSpecificationList(specification);
        ExcelUtil<Specification> util = new ExcelUtil<Specification>(Specification.class);
        util.exportExcel(response, list, "规格管理数据");
    }

    /**
     * 获取规格管理详细信息
     */
    @PreAuthorize("@ss.hasPermi('product:specification:query')")
    @GetMapping(value = "/{specId}")
    public AjaxResult getInfo(@PathVariable("specId") Long specId)
    {
        return success(specificationService.selectSpecificationBySpecId(specId));
    }

    /**
     * 新增规格管理
     */
    @PreAuthorize("@ss.hasPermi('product:specification:add')")
    @Log(title = "规格管理", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody Specification specification)
    {
        return toAjax(specificationService.insertSpecification(specification));
    }

    /**
     * 修改规格管理
     */
    @PreAuthorize("@ss.hasPermi('product:specification:edit')")
    @Log(title = "规格管理", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody Specification specification)
    {
        return toAjax(specificationService.updateSpecification(specification));
    }

    /**
     * 删除规格管理
     */
    @PreAuthorize("@ss.hasPermi('product:specification:remove')")
    @Log(title = "规格管理", businessType = BusinessType.DELETE)
	@DeleteMapping("/{specIds}")
    public AjaxResult remove(@PathVariable Long[] specIds)
    {
        return toAjax(specificationService.deleteSpecificationBySpecIds(specIds));
    }
}
