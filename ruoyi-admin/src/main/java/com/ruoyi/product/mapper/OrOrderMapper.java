package com.ruoyi.product.mapper;

import java.util.List;
import com.ruoyi.product.domain.OrOrder;

/**
 * 订单Mapper接口
 * 
 * @author ruoyi
 * @date 2024-01-03
 */
public interface OrOrderMapper 
{
    /**
     * 查询订单
     * 
     * @param orderId 订单主键
     * @return 订单
     */
    public OrOrder selectOrOrderByOrderId(Long orderId);

    /**
     * 查询订单列表
     * 
     * @param orOrder 订单
     * @return 订单集合
     */
    public List<OrOrder> selectOrOrderList(OrOrder orOrder);

    /**
     * 新增订单
     * 
     * @param orOrder 订单
     * @return 结果
     */
    public int insertOrOrder(OrOrder orOrder);

    /**
     * 修改订单
     * 
     * @param orOrder 订单
     * @return 结果
     */
    public int updateOrOrder(OrOrder orOrder);

    /**
     * 删除订单
     * 
     * @param orderId 订单主键
     * @return 结果
     */
    public int deleteOrOrderByOrderId(Long orderId);

    /**
     * 批量删除订单
     * 
     * @param orderIds 需要删除的数据主键集合
     * @return 结果
     */
    public int deleteOrOrderByOrderIds(Long[] orderIds);
}
