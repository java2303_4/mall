package com.ruoyi.product.mapper;

import java.util.List;
import com.ruoyi.product.domain.Group;

/**
 * 分组管理Mapper接口
 * 
 * @author ruoyi
 * @date 2024-01-03
 */
public interface GroupMapper 
{
    /**
     * 查询分组管理
     * 
     * @param groupId 分组管理主键
     * @return 分组管理
     */
    public Group selectGroupByGroupId(Long groupId);

    /**
     * 查询分组管理列表
     * 
     * @param group 分组管理
     * @return 分组管理集合
     */
    public List<Group> selectGroupList(Group group);

    /**
     * 新增分组管理
     * 
     * @param group 分组管理
     * @return 结果
     */
    public int insertGroup(Group group);

    /**
     * 修改分组管理
     * 
     * @param group 分组管理
     * @return 结果
     */
    public int updateGroup(Group group);

    /**
     * 删除分组管理
     * 
     * @param groupId 分组管理主键
     * @return 结果
     */
    public int deleteGroupByGroupId(Long groupId);

    /**
     * 批量删除分组管理
     * 
     * @param groupIds 需要删除的数据主键集合
     * @return 结果
     */
    public int deleteGroupByGroupIds(Long[] groupIds);
}
