package com.ruoyi.product.mapper;

import java.util.List;
import com.ruoyi.product.domain.Specification;
import com.ruoyi.product.domain.ProSpecificationProduct;

/**
 * 规格管理Mapper接口
 * 
 * @author ruoyi
 * @date 2024-01-03
 */
public interface SpecificationMapper 
{
    /**
     * 查询规格管理
     * 
     * @param specId 规格管理主键
     * @return 规格管理
     */
    public Specification selectSpecificationBySpecId(Long specId);

    /**
     * 查询规格管理列表
     * 
     * @param specification 规格管理
     * @return 规格管理集合
     */
    public List<Specification> selectSpecificationList(Specification specification);

    /**
     * 新增规格管理
     * 
     * @param specification 规格管理
     * @return 结果
     */
    public int insertSpecification(Specification specification);

    /**
     * 修改规格管理
     * 
     * @param specification 规格管理
     * @return 结果
     */
    public int updateSpecification(Specification specification);

    /**
     * 删除规格管理
     * 
     * @param specId 规格管理主键
     * @return 结果
     */
    public int deleteSpecificationBySpecId(Long specId);

    /**
     * 批量删除规格管理
     * 
     * @param specIds 需要删除的数据主键集合
     * @return 结果
     */
    public int deleteSpecificationBySpecIds(Long[] specIds);

    /**
     * 批量删除${subTable.functionName}
     * 
     * @param specIds 需要删除的数据主键集合
     * @return 结果
     */
    public int deleteProSpecificationProductBySpecIds(Long[] specIds);
    
    /**
     * 批量新增${subTable.functionName}
     * 
     * @param proSpecificationProductList ${subTable.functionName}列表
     * @return 结果
     */
    public int batchProSpecificationProduct(List<ProSpecificationProduct> proSpecificationProductList);
    

    /**
     * 通过规格管理主键删除${subTable.functionName}信息
     * 
     * @param specId 规格管理ID
     * @return 结果
     */
    public int deleteProSpecificationProductBySpecId(Long specId);
}
