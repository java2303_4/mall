package com.ruoyi.product.mapper;

import java.util.List;
import com.ruoyi.product.domain.SpecificationProduct;

/**
 * 规格属性中间表Mapper接口
 * 
 * @author ruoyi
 * @date 2024-01-07
 */
public interface SpecificationProductMapper 
{
    /**
     * 查询规格属性中间表
     * 
     * @param valueId 规格属性中间表主键
     * @return 规格属性中间表
     */
    public SpecificationProduct selectSpecificationProductByValueId(Long valueId);

    public SpecificationProduct selectSpecificationProductBySpecId(Long specId);

    /**
     * 查询规格属性中间表列表
     * 
     * @param specificationProduct 规格属性中间表
     * @return 规格属性中间表集合
     */
    public List<SpecificationProduct> selectSpecificationProductList(SpecificationProduct specificationProduct);

    /**
     * 新增规格属性中间表
     * 
     * @param specificationProduct 规格属性中间表
     * @return 结果
     */
    public int insertSpecificationProduct(SpecificationProduct specificationProduct);

    /**
     * 修改规格属性中间表
     * 
     * @param specificationProduct 规格属性中间表
     * @return 结果
     */
    public int updateSpecificationProduct(SpecificationProduct specificationProduct);

    /**
     * 删除规格属性中间表
     * 
     * @param valueId 规格属性中间表主键
     * @return 结果
     */
    public int deleteSpecificationProductByValueId(Long valueId);

    /**
     * 批量删除规格属性中间表
     * 
     * @param valueIds 需要删除的数据主键集合
     * @return 结果
     */
    public int deleteSpecificationProductByValueIds(Long[] valueIds);
}
