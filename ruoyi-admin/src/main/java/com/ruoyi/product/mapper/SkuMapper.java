package com.ruoyi.product.mapper;

import java.util.List;
import com.ruoyi.product.domain.Sku;

/**
 * 商品SKUMapper接口
 * 
 * @author ruoyi
 * @date 2024-01-09
 */
public interface SkuMapper 
{
    /**
     * 查询商品SKU
     * 
     * @param skuId 商品SKU主键
     * @return 商品SKU
     */
    public Sku selectSkuBySkuId(Long skuId);
    public List<Sku> selectSkuProductId(Long productId);

    /**
     * 查询商品SKU列表
     * 
     * @param sku 商品SKU
     * @return 商品SKU集合
     */
    public List<Sku> selectSkuList(Sku sku);


    /**
     * 新增商品SKU
     * 
     * @param sku 商品SKU
     * @return 结果
     */
    public int insertSku(Sku sku);

    /**
     * 修改商品SKU
     * 
     * @param sku 商品SKU
     * @return 结果
     */
    public int updateSku(Sku sku);

    /**
     * 删除商品SKU
     * 
     * @param skuId 商品SKU主键
     * @return 结果
     */
    public int deleteSkuBySkuId(Long skuId);

    /**
     * 批量删除商品SKU
     * 
     * @param skuIds 需要删除的数据主键集合
     * @return 结果
     */
    public int deleteSkuBySkuIds(Long[] skuIds);
    /**
     * 根据商品id获取商品sku列表
     * @param prodId
     * @return
     */
    List<Sku> listByProdId(Long prodId);
}
