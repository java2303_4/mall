package com.ruoyi.product.service.impl;

import java.util.List;
import com.ruoyi.common.utils.DateUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ruoyi.product.mapper.GroupMapper;
import com.ruoyi.product.domain.Group;
import com.ruoyi.product.service.IGroupService;

/**
 * 分组管理Service业务层处理
 * 
 * @author ruoyi
 * @date 2024-01-03
 */
@Service
public class GroupServiceImpl implements IGroupService 
{
    @Autowired
    private GroupMapper groupMapper;

    /**
     * 查询分组管理
     * 
     * @param groupId 分组管理主键
     * @return 分组管理
     */
    @Override
    public Group selectGroupByGroupId(Long groupId)
    {
        return groupMapper.selectGroupByGroupId(groupId);
    }

    /**
     * 查询分组管理列表
     * 
     * @param group 分组管理
     * @return 分组管理
     */
    @Override
    public List<Group> selectGroupList(Group group)
    {
        return groupMapper.selectGroupList(group);
    }

    /**
     * 新增分组管理
     * 
     * @param group 分组管理
     * @return 结果
     */
    @Override
    public int insertGroup(Group group)
    {
        group.setCreateTime(DateUtils.getNowDate());
        return groupMapper.insertGroup(group);
    }

    /**
     * 修改分组管理
     * 
     * @param group 分组管理
     * @return 结果
     */
    @Override
    public int updateGroup(Group group)
    {
        group.setUpdateTime(DateUtils.getNowDate());
        return groupMapper.updateGroup(group);
    }

    /**
     * 批量删除分组管理
     * 
     * @param groupIds 需要删除的分组管理主键
     * @return 结果
     */
    @Override
    public int deleteGroupByGroupIds(Long[] groupIds)
    {
        return groupMapper.deleteGroupByGroupIds(groupIds);
    }

    /**
     * 删除分组管理信息
     * 
     * @param groupId 分组管理主键
     * @return 结果
     */
    @Override
    public int deleteGroupByGroupId(Long groupId)
    {
        return groupMapper.deleteGroupByGroupId(groupId);
    }
}
