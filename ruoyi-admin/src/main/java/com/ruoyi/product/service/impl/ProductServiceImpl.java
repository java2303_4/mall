package com.ruoyi.product.service.impl;

import java.util.List;
import com.ruoyi.common.utils.DateUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import java.util.ArrayList;
import com.ruoyi.common.utils.StringUtils;
import org.springframework.transaction.annotation.Transactional;
import com.ruoyi.product.domain.Specification;
import com.ruoyi.product.mapper.ProductMapper;
import com.ruoyi.product.domain.Product;
import com.ruoyi.product.service.IProductService;

/**
 * 产品管理Service业务层处理
 * 
 * @author ruoyi
 * @date 2024-01-07
 */
@Service
public class ProductServiceImpl implements IProductService 
{
    @Autowired
    private ProductMapper productMapper;

    /**
     * 查询产品管理
     * 
     * @param productId 产品管理主键
     * @return 产品管理
     */
    @Override
    public Product selectProductByProductId(Long productId)
    {
        return productMapper.selectProductByProductId(productId);
    }

    /**
     * 查询产品管理列表
     * 
     * @param product 产品管理
     * @return 产品管理
     */
    @Override
    public List<Product> selectProductList(Product product)
    {
        return productMapper.selectProductList(product);
    }

    /**
     * 新增产品管理
     * 
     * @param product 产品管理
     * @return 结果
     */
    @Transactional
    @Override
    public int insertProduct(Product product)
    {
        product.setCreateTime(DateUtils.getNowDate());
        int rows = productMapper.insertProduct(product);
        insertSpecification(product);
        return rows;
    }

    /**
     * 修改产品管理
     * 
     * @param product 产品管理
     * @return 结果
     */
    @Transactional
    @Override
    public int updateProduct(Product product)
    {
        product.setUpdateTime(DateUtils.getNowDate());
        productMapper.deleteSpecificationBySpecId(product.getProductId());
        insertSpecification(product);
        return productMapper.updateProduct(product);
    }

    /**
     * 批量删除产品管理
     * 
     * @param productIds 需要删除的产品管理主键
     * @return 结果
     */
    @Transactional
    @Override
    public int deleteProductByProductIds(Long[] productIds)
    {
        productMapper.deleteSpecificationBySpecIds(productIds);
        return productMapper.deleteProductByProductIds(productIds);
    }

    /**
     * 删除产品管理信息
     * 
     * @param productId 产品管理主键
     * @return 结果
     */
    @Transactional
    @Override
    public int deleteProductByProductId(Long productId)
    {
        productMapper.deleteSpecificationBySpecId(productId);
        return productMapper.deleteProductByProductId(productId);
    }

    /**
     * 新增规格管理信息
     * 
     * @param product 产品管理对象
     */
    public void insertSpecification(Product product)
    {
        List<Specification> specificationList = product.getSpecificationList();
        Long productId = product.getProductId();
        if (StringUtils.isNotNull(specificationList))
        {
            List<Specification> list = new ArrayList<Specification>();
            for (Specification specification : specificationList)
            {
                specification.setSpecId(productId);
                list.add(specification);
            }
            if (list.size() > 0)
            {
                productMapper.batchSpecification(list);
            }
        }
    }
    @Override
    public Product getProductByProdId(Long prodId) {
        return productMapper.selectProductByProductId(prodId);
    }
}
