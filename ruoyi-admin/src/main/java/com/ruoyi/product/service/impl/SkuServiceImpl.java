package com.ruoyi.product.service.impl;

import java.util.List;
import com.ruoyi.common.utils.DateUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ruoyi.product.mapper.SkuMapper;
import com.ruoyi.product.domain.Sku;
import com.ruoyi.product.service.ISkuService;

/**
 * 商品SKUService业务层处理
 * 
 * @author ruoyi
 * @date 2024-01-09
 */
@Service
public class SkuServiceImpl implements ISkuService 
{
    @Autowired
    private SkuMapper skuMapper;

    /**
     * 查询商品SKU
     * 
     * @param skuId 商品SKU主键
     * @return 商品SKU
     */
    @Override
    public Sku selectSkuBySkuId(Long skuId)
    {
        return skuMapper.selectSkuBySkuId(skuId);
    }

    /**
     * 查询商品SKU列表
     * 
     * @param sku 商品SKU
     * @return 商品SKU
     */
    @Override
    public List<Sku> selectSkuList(Sku sku)
    {
        return skuMapper.selectSkuList(sku);
    }

    /**
     * 新增商品SKU
     * 
     * @param sku 商品SKU
     * @return 结果
     */
    @Override
    public int insertSku(Sku sku)
    {
        return skuMapper.insertSku(sku);
    }

    /**
     * 修改商品SKU
     * 
     * @param sku 商品SKU
     * @return 结果
     */
    @Override
    public int updateSku(Sku sku)
    {
        sku.setUpdateTime(DateUtils.getNowDate());
        return skuMapper.updateSku(sku);
    }

    /**
     * 批量删除商品SKU
     * 
     * @param skuIds 需要删除的商品SKU主键
     * @return 结果
     */
    @Override
    public int deleteSkuBySkuIds(Long[] skuIds)
    {
        return skuMapper.deleteSkuBySkuIds(skuIds);
    }

    /**
     * 删除商品SKU信息
     * 
     * @param skuId 商品SKU主键
     * @return 结果
     */
    @Override
    public int deleteSkuBySkuId(Long skuId)
    {
        return skuMapper.deleteSkuBySkuId(skuId);
    }

    @Override
    public Sku getSkuBySkuId(Long skuId) {
        return skuMapper.selectSkuBySkuId(skuId);
    }
}
