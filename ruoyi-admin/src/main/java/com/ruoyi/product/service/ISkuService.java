package com.ruoyi.product.service;

import java.util.List;
import com.ruoyi.product.domain.Sku;

/**
 * 商品SKUService接口
 * 
 * @author ruoyi
 * @date 2024-01-09
 */
public interface ISkuService 
{
    /**
     * 查询商品SKU
     * 
     * @param skuId 商品SKU主键
     * @return 商品SKU
     */
    public Sku selectSkuBySkuId(Long skuId);

    /**
     * 查询商品SKU列表
     * 
     * @param sku 商品SKU
     * @return 商品SKU集合
     */
    public List<Sku> selectSkuList(Sku sku);

    /**
     * 新增商品SKU
     * 
     * @param sku 商品SKU
     * @return 结果
     */
    public int insertSku(Sku sku);

    /**
     * 修改商品SKU
     * 
     * @param sku 商品SKU
     * @return 结果
     */
    public int updateSku(Sku sku);

    /**
     * 批量删除商品SKU
     * 
     * @param skuIds 需要删除的商品SKU主键集合
     * @return 结果
     */
    public int deleteSkuBySkuIds(Long[] skuIds);

    /**
     * 删除商品SKU信息
     * 
     * @param skuId 商品SKU主键
     * @return 结果
     */
    public int deleteSkuBySkuId(Long skuId);
    /**
     * 根据skuId获取sku信息（将会被缓存起来）
     * @param skuId
     * @return
     */
    Sku getSkuBySkuId(Long skuId);
}
