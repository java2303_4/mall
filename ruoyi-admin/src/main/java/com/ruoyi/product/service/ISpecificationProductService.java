package com.ruoyi.product.service;

import java.util.List;
import com.ruoyi.product.domain.SpecificationProduct;

/**
 * 规格属性中间表Service接口
 * 
 * @author ruoyi
 * @date 2024-01-07
 */
public interface ISpecificationProductService 
{
    /**
     * 查询规格属性中间表
     * 
     * @param valueId 规格属性中间表主键
     * @return 规格属性中间表
     */
    public SpecificationProduct selectSpecificationProductByValueId(Long valueId);

    /**
     * 查询规格属性中间表列表
     * 
     * @param specificationProduct 规格属性中间表
     * @return 规格属性中间表集合
     */
    public List<SpecificationProduct> selectSpecificationProductList(SpecificationProduct specificationProduct);

    /**
     * 新增规格属性中间表
     * 
     * @param specificationProduct 规格属性中间表
     * @return 结果
     */
    public int insertSpecificationProduct(SpecificationProduct specificationProduct);

    /**
     * 修改规格属性中间表
     * 
     * @param specificationProduct 规格属性中间表
     * @return 结果
     */
    public int updateSpecificationProduct(SpecificationProduct specificationProduct);

    /**
     * 批量删除规格属性中间表
     * 
     * @param valueIds 需要删除的规格属性中间表主键集合
     * @return 结果
     */
    public int deleteSpecificationProductByValueIds(Long[] valueIds);

    /**
     * 删除规格属性中间表信息
     * 
     * @param valueId 规格属性中间表主键
     * @return 结果
     */
    public int deleteSpecificationProductByValueId(Long valueId);
}
