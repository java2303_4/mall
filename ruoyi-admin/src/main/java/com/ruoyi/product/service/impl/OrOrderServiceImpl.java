package com.ruoyi.product.service.impl;

import java.util.List;
import com.ruoyi.common.utils.DateUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ruoyi.product.mapper.OrOrderMapper;
import com.ruoyi.product.domain.OrOrder;
import com.ruoyi.product.service.IOrOrderService;

/**
 * 订单Service业务层处理
 * 
 * @author ruoyi
 * @date 2024-01-03
 */
@Service
public class OrOrderServiceImpl implements IOrOrderService 
{
    @Autowired
    private OrOrderMapper orOrderMapper;

    /**
     * 查询订单
     * 
     * @param orderId 订单主键
     * @return 订单
     */
    @Override
    public OrOrder selectOrOrderByOrderId(Long orderId)
    {
        return orOrderMapper.selectOrOrderByOrderId(orderId);
    }

    /**
     * 查询订单列表
     * 
     * @param orOrder 订单
     * @return 订单
     */
    @Override
    public List<OrOrder> selectOrOrderList(OrOrder orOrder)
    {
        return orOrderMapper.selectOrOrderList(orOrder);
    }

    /**
     * 新增订单
     * 
     * @param orOrder 订单
     * @return 结果
     */
    @Override
    public int insertOrOrder(OrOrder orOrder)
    {
        orOrder.setCreateTime(DateUtils.getNowDate());
        return orOrderMapper.insertOrOrder(orOrder);
    }

    /**
     * 修改订单
     * 
     * @param orOrder 订单
     * @return 结果
     */
    @Override
    public int updateOrOrder(OrOrder orOrder)
    {
        orOrder.setUpdateTime(DateUtils.getNowDate());
        return orOrderMapper.updateOrOrder(orOrder);
    }

    /**
     * 批量删除订单
     * 
     * @param orderIds 需要删除的订单主键
     * @return 结果
     */
    @Override
    public int deleteOrOrderByOrderIds(Long[] orderIds)
    {
        return orOrderMapper.deleteOrOrderByOrderIds(orderIds);
    }

    /**
     * 删除订单信息
     * 
     * @param orderId 订单主键
     * @return 结果
     */
    @Override
    public int deleteOrOrderByOrderId(Long orderId)
    {
        return orOrderMapper.deleteOrOrderByOrderId(orderId);
    }
}
