package com.ruoyi.product.service.impl;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import java.util.ArrayList;
import com.ruoyi.common.utils.StringUtils;
import org.springframework.transaction.annotation.Transactional;
import com.ruoyi.product.domain.ProSpecificationProduct;
import com.ruoyi.product.mapper.SpecificationMapper;
import com.ruoyi.product.domain.Specification;
import com.ruoyi.product.service.ISpecificationService;

/**
 * 规格管理Service业务层处理
 * 
 * @author ruoyi
 * @date 2024-01-03
 */
@Service
public class SpecificationServiceImpl implements ISpecificationService 
{
    @Autowired
    private SpecificationMapper specificationMapper;

    /**
     * 查询规格管理
     * 
     * @param specId 规格管理主键
     * @return 规格管理
     */
    @Override
    public Specification selectSpecificationBySpecId(Long specId)
    {
        return specificationMapper.selectSpecificationBySpecId(specId);
    }

    /**
     * 查询规格管理列表
     * 
     * @param specification 规格管理
     * @return 规格管理
     */
    @Override
    public List<Specification> selectSpecificationList(Specification specification)
    {
        return specificationMapper.selectSpecificationList(specification);
    }

    /**
     * 新增规格管理
     * 
     * @param specification 规格管理
     * @return 结果
     */
    @Transactional
    @Override
    public int insertSpecification(Specification specification)
    {
        int rows = specificationMapper.insertSpecification(specification);
        insertProSpecificationProduct(specification);
        return rows;
    }

    /**
     * 修改规格管理
     * 
     * @param specification 规格管理
     * @return 结果
     */
    @Transactional
    @Override
    public int updateSpecification(Specification specification)
    {
        specificationMapper.deleteProSpecificationProductBySpecId(specification.getSpecId());
        insertProSpecificationProduct(specification);
        return specificationMapper.updateSpecification(specification);
    }

    /**
     * 批量删除规格管理
     * 
     * @param specIds 需要删除的规格管理主键
     * @return 结果
     */
    @Transactional
    @Override
    public int deleteSpecificationBySpecIds(Long[] specIds)
    {
        specificationMapper.deleteProSpecificationProductBySpecIds(specIds);
        return specificationMapper.deleteSpecificationBySpecIds(specIds);
    }

    /**
     * 删除规格管理信息
     * 
     * @param specId 规格管理主键
     * @return 结果
     */
    @Transactional
    @Override
    public int deleteSpecificationBySpecId(Long specId)
    {
        specificationMapper.deleteProSpecificationProductBySpecId(specId);
        return specificationMapper.deleteSpecificationBySpecId(specId);
    }

    /**
     * 新增${subTable.functionName}信息
     * 
     * @param specification 规格管理对象
     */
    public void insertProSpecificationProduct(Specification specification)
    {
        List<ProSpecificationProduct> proSpecificationProductList = specification.getProSpecificationProductList();
        Long specId = specification.getSpecId();
        if (StringUtils.isNotNull(proSpecificationProductList))
        {
            List<ProSpecificationProduct> list = new ArrayList<ProSpecificationProduct>();
            for (ProSpecificationProduct proSpecificationProduct : proSpecificationProductList)
            {
                proSpecificationProduct.setSpecId(specId);
                list.add(proSpecificationProduct);
            }
            if (list.size() > 0)
            {
                specificationMapper.batchProSpecificationProduct(list);
            }
        }
    }
}
