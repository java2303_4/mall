package com.ruoyi.product.service;

import java.util.List;
import com.ruoyi.product.domain.Specification;

/**
 * 规格管理Service接口
 * 
 * @author ruoyi
 * @date 2024-01-03
 */
public interface ISpecificationService 
{
    /**
     * 查询规格管理
     * 
     * @param specId 规格管理主键
     * @return 规格管理
     */
    public Specification selectSpecificationBySpecId(Long specId);

    /**
     * 查询规格管理列表
     * 
     * @param specification 规格管理
     * @return 规格管理集合
     */
    public List<Specification> selectSpecificationList(Specification specification);

    /**
     * 新增规格管理
     * 
     * @param specification 规格管理
     * @return 结果
     */
    public int insertSpecification(Specification specification);

    /**
     * 修改规格管理
     * 
     * @param specification 规格管理
     * @return 结果
     */
    public int updateSpecification(Specification specification);

    /**
     * 批量删除规格管理
     * 
     * @param specIds 需要删除的规格管理主键集合
     * @return 结果
     */
    public int deleteSpecificationBySpecIds(Long[] specIds);

    /**
     * 删除规格管理信息
     * 
     * @param specId 规格管理主键
     * @return 结果
     */
    public int deleteSpecificationBySpecId(Long specId);
}
