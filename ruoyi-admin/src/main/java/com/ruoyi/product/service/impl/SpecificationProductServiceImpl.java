package com.ruoyi.product.service.impl;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ruoyi.product.mapper.SpecificationProductMapper;
import com.ruoyi.product.domain.SpecificationProduct;
import com.ruoyi.product.service.ISpecificationProductService;

/**
 * 规格属性中间表Service业务层处理
 * 
 * @author ruoyi
 * @date 2024-01-07
 */
@Service
public class SpecificationProductServiceImpl implements ISpecificationProductService 
{
    @Autowired
    private SpecificationProductMapper specificationProductMapper;

    /**
     * 查询规格属性中间表
     * 
     * @param valueId 规格属性中间表主键
     * @return 规格属性中间表
     */
    @Override
    public SpecificationProduct selectSpecificationProductByValueId(Long valueId)
    {
        return specificationProductMapper.selectSpecificationProductByValueId(valueId);
    }

    /**
     * 查询规格属性中间表列表
     * 
     * @param specificationProduct 规格属性中间表
     * @return 规格属性中间表
     */
    @Override
    public List<SpecificationProduct> selectSpecificationProductList(SpecificationProduct specificationProduct)
    {
        return specificationProductMapper.selectSpecificationProductList(specificationProduct);
    }

    /**
     * 新增规格属性中间表
     * 
     * @param specificationProduct 规格属性中间表
     * @return 结果
     */
    @Override
    public int insertSpecificationProduct(SpecificationProduct specificationProduct)
    {
        return specificationProductMapper.insertSpecificationProduct(specificationProduct);
    }

    /**
     * 修改规格属性中间表
     * 
     * @param specificationProduct 规格属性中间表
     * @return 结果
     */
    @Override
    public int updateSpecificationProduct(SpecificationProduct specificationProduct)
    {
        return specificationProductMapper.updateSpecificationProduct(specificationProduct);
    }

    /**
     * 批量删除规格属性中间表
     * 
     * @param valueIds 需要删除的规格属性中间表主键
     * @return 结果
     */
    @Override
    public int deleteSpecificationProductByValueIds(Long[] valueIds)
    {
        return specificationProductMapper.deleteSpecificationProductByValueIds(valueIds);
    }

    /**
     * 删除规格属性中间表信息
     * 
     * @param valueId 规格属性中间表主键
     * @return 结果
     */
    @Override
    public int deleteSpecificationProductByValueId(Long valueId)
    {
        return specificationProductMapper.deleteSpecificationProductByValueId(valueId);
    }
}
