package com.ruoyi.product.domain;

import java.util.List;
import java.util.Date;
import com.fasterxml.jackson.annotation.JsonFormat;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.ruoyi.common.annotation.Excel;
import com.ruoyi.common.core.domain.BaseEntity;

/**
 * 产品管理对象 pro_product
 * 
 * @author ruoyi
 * @date 2024-01-07
 */
public class Product extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** 产品ID */
    private Long productId;

    /** 产品名字 */
    @Excel(name = "产品名字")
    private String productName;

    /** 商品原价 */
    @Excel(name = "商品原价")
    private Long originalPrice;

    /** 商品现价 */
    @Excel(name = "商品现价")
    private Long currentPrice;

    /** 商品库存 */
    @Excel(name = "商品库存")
    private Long inventory;

    /** 商品主图 */
    @Excel(name = "商品主图")
    private String pic;

    /** 产品状态（0 上架 1 未上架） */
    @Excel(name = "产品状态", readConverterExp = "0=,上=架,1=,未=上架")
    private String status;

    /** 分类ID */
    @Excel(name = "分类ID")
    private Long categoryId;

    /** 分组ID */
    @Excel(name = "分组ID")
    private Long groupId;

    /** 简要描述 */
    @Excel(name = "简要描述")
    private String brief;

    /** 配送方式（0 商家配送 1 自提） */
    @Excel(name = "配送方式", readConverterExp = "0=,商=家配送,1=,自=提")
    private String deliveryType;

    /** 产品详情 */
    @Excel(name = "产品详情")
    private String details;

    /** 店铺ID */
    @Excel(name = "店铺ID")
    private Long shopId;

    /** 上架时间 */
    @JsonFormat(pattern = "yyyy-MM-dd")
    @Excel(name = "上架时间", width = 30, dateFormat = "yyyy-MM-dd")
    private Date putAwayTime;

    /** 销量 */
    @Excel(name = "销量")
    private Long soldNum;

    /** 详细描述 */
    @Excel(name = "详细描述")
    private String content;

    /** 商品图片（以,分割） */
    @Excel(name = "商品图片", readConverterExp = "以=,分割")
    private String images;

    /** 运费模板ID */
    @Excel(name = "运费模板ID")
    private Long deliveryTemplateId;

    /** 规格管理信息 */
    private List<Specification> specificationList;


    private List<Sku> skuAndProduct;

    public List<Sku> getSkuAndProduct() {
        return skuAndProduct;
    }

    public void setSkuAndProduct(List<Sku> skuAndProduct) {
        this.skuAndProduct = skuAndProduct;
    }

    public void setProductId(Long productId)
    {
        this.productId = productId;
    }

    public Long getProductId() 
    {
        return productId;
    }
    public void setProductName(String productName) 
    {
        this.productName = productName;
    }

    public String getProductName() 
    {
        return productName;
    }
    public void setOriginalPrice(Long originalPrice) 
    {
        this.originalPrice = originalPrice;
    }

    public Long getOriginalPrice() 
    {
        return originalPrice;
    }
    public void setCurrentPrice(Long currentPrice) 
    {
        this.currentPrice = currentPrice;
    }

    public Long getCurrentPrice() 
    {
        return currentPrice;
    }
    public void setInventory(Long inventory) 
    {
        this.inventory = inventory;
    }

    public Long getInventory() 
    {
        return inventory;
    }
    public void setPic(String pic) 
    {
        this.pic = pic;
    }

    public String getPic() 
    {
        return pic;
    }
    public void setStatus(String status) 
    {
        this.status = status;
    }

    public String getStatus() 
    {
        return status;
    }
    public void setCategoryId(Long categoryId) 
    {
        this.categoryId = categoryId;
    }

    public Long getCategoryId() 
    {
        return categoryId;
    }
    public void setGroupId(Long groupId) 
    {
        this.groupId = groupId;
    }

    public Long getGroupId() 
    {
        return groupId;
    }
    public void setBrief(String brief) 
    {
        this.brief = brief;
    }

    public String getBrief() 
    {
        return brief;
    }
    public void setDeliveryType(String deliveryType) 
    {
        this.deliveryType = deliveryType;
    }

    public String getDeliveryType() 
    {
        return deliveryType;
    }
    public void setDetails(String details) 
    {
        this.details = details;
    }

    public String getDetails() 
    {
        return details;
    }
    public void setShopId(Long shopId) 
    {
        this.shopId = shopId;
    }

    public Long getShopId() 
    {
        return shopId;
    }
    public void setPutAwayTime(Date putAwayTime) 
    {
        this.putAwayTime = putAwayTime;
    }

    public Date getPutAwayTime() 
    {
        return putAwayTime;
    }
    public void setSoldNum(Long soldNum) 
    {
        this.soldNum = soldNum;
    }

    public Long getSoldNum() 
    {
        return soldNum;
    }
    public void setContent(String content) 
    {
        this.content = content;
    }

    public String getContent() 
    {
        return content;
    }
    public void setImages(String images) 
    {
        this.images = images;
    }

    public String getImages() 
    {
        return images;
    }
    public void setDeliveryTemplateId(Long deliveryTemplateId) 
    {
        this.deliveryTemplateId = deliveryTemplateId;
    }

    public Long getDeliveryTemplateId() 
    {
        return deliveryTemplateId;
    }

    public List<Specification> getSpecificationList()
    {
        return specificationList;
    }

    public void setSpecificationList(List<Specification> specificationList)
    {
        this.specificationList = specificationList;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("productId", getProductId())
            .append("productName", getProductName())
            .append("originalPrice", getOriginalPrice())
            .append("currentPrice", getCurrentPrice())
            .append("inventory", getInventory())
            .append("pic", getPic())
            .append("status", getStatus())
            .append("categoryId", getCategoryId())
            .append("groupId", getGroupId())
            .append("brief", getBrief())
            .append("deliveryType", getDeliveryType())
            .append("details", getDetails())
            .append("createBy", getCreateBy())
            .append("createTime", getCreateTime())
            .append("updateBy", getUpdateBy())
            .append("updateTime", getUpdateTime())
            .append("shopId", getShopId())
            .append("putAwayTime", getPutAwayTime())
            .append("soldNum", getSoldNum())
            .append("content", getContent())
            .append("images", getImages())
            .append("deliveryTemplateId", getDeliveryTemplateId())
            .append("specificationList", getSpecificationList())
            .toString();
    }
}
