package com.ruoyi.product.domain;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.ruoyi.common.annotation.Excel;
import com.ruoyi.common.core.domain.BaseEntity;

/**
 * 规格属性中间表对象 pro_specification_product
 * 
 * @author ruoyi
 * @date 2024-01-07
 */
public class SpecificationProduct extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** 属性值ID */
    private Long valueId;

    /** 属性值名称 */
    @Excel(name = "属性值名称")
    private String valueName;

    /** 属性ID */
    @Excel(name = "属性ID")
    private Long specId;

    public void setValueId(Long valueId) 
    {
        this.valueId = valueId;
    }

    public Long getValueId() 
    {
        return valueId;
    }
    public void setValueName(String valueName) 
    {
        this.valueName = valueName;
    }

    public String getValueName() 
    {
        return valueName;
    }
    public void setSpecId(Long specId) 
    {
        this.specId = specId;
    }

    public Long getSpecId() 
    {
        return specId;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("valueId", getValueId())
            .append("valueName", getValueName())
            .append("specId", getSpecId())
            .toString();
    }
}
