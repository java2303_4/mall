package com.ruoyi.product.domain;

import java.util.Date;
import com.fasterxml.jackson.annotation.JsonFormat;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.ruoyi.common.annotation.Excel;
import com.ruoyi.common.core.domain.BaseEntity;

/**
 * 分组管理对象 pro_group
 * 
 * @author ruoyi
 * @date 2024-01-03
 */
public class Group extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** ID */
    private Long groupId;

    /** 标签名称 */
    @Excel(name = "标签名称")
    private String groupName;

    /** 状态（0 正常 1 禁用） */
    @Excel(name = "状态", readConverterExp = "0=,正=常,1=,禁=用")
    private String status;

    /** 排序 */
    @Excel(name = "排序")
    private String seq;

    /** 店铺ID */
    private Long shopId;

    /** 默认类型（0 商家自定义 1 系统默认） */
    @Excel(name = "默认类型", readConverterExp = "0=,商=家自定义,1=,系=统默认")
    private Integer isDefault;

    /** 商品数量 */
    private Long productCount;

    /** 列表样式（0 一列一个 1 一列两个 2 一列三个） */
    private Long style;

    /** 删除时间 */
    private Date deleteTime;

    public void setGroupId(Long groupId) 
    {
        this.groupId = groupId;
    }

    public Long getGroupId() 
    {
        return groupId;
    }
    public void setGroupName(String groupName) 
    {
        this.groupName = groupName;
    }

    public String getGroupName() 
    {
        return groupName;
    }
    public void setStatus(String status) 
    {
        this.status = status;
    }

    public String getStatus() 
    {
        return status;
    }
    public void setSeq(String seq) 
    {
        this.seq = seq;
    }

    public String getSeq() 
    {
        return seq;
    }
    public void setShopId(Long shopId) 
    {
        this.shopId = shopId;
    }

    public Long getShopId() 
    {
        return shopId;
    }
    public void setIsDefault(Integer isDefault) 
    {
        this.isDefault = isDefault;
    }

    public Integer getIsDefault() 
    {
        return isDefault;
    }
    public void setProductCount(Long productCount) 
    {
        this.productCount = productCount;
    }

    public Long getProductCount() 
    {
        return productCount;
    }
    public void setStyle(Long style) 
    {
        this.style = style;
    }

    public Long getStyle() 
    {
        return style;
    }
    public void setDeleteTime(Date deleteTime) 
    {
        this.deleteTime = deleteTime;
    }

    public Date getDeleteTime() 
    {
        return deleteTime;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("groupId", getGroupId())
            .append("groupName", getGroupName())
            .append("status", getStatus())
            .append("seq", getSeq())
            .append("shopId", getShopId())
            .append("isDefault", getIsDefault())
            .append("productCount", getProductCount())
            .append("style", getStyle())
            .append("createTime", getCreateTime())
            .append("updateTime", getUpdateTime())
            .append("deleteTime", getDeleteTime())
            .toString();
    }
}
