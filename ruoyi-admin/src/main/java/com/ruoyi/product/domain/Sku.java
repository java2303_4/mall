package com.ruoyi.product.domain;

import java.math.BigDecimal;
import java.util.Date;
import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.ruoyi.common.annotation.Excel;
import com.ruoyi.common.core.domain.BaseEntity;

/**
 * 商品SKU对象 pro_sku
 * 
 * @author ruoyi
 * @date 2024-01-09
 */
@Data
public class Sku extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** 单品ID */
    private Long skuId;

    /** 产品ID */
    @Excel(name = "产品ID")
    private Long productId;

    /** 销售属性组合字符串 格式是p1:v1;p2:v2 */
    @Excel(name = "销售属性组合字符串 格式是p1:v1;p2:v2")
    private String properties;

    /** 原价 */
    @Excel(name = "原价")
    private Double oriPrice;

    /** 价格 */
    @Excel(name = "价格")
    private Double price;

    /** 商品在付款减库存的状态下，该sku上未付款的订单数量 */
    @Excel(name = "商品在付款减库存的状态下，该sku上未付款的订单数量")
    private Long stocks;

    /** 实际库存 */
    @Excel(name = "实际库存")
    private Long actualStocks;

    /** 记录时间 */
    @JsonFormat(pattern = "yyyy-MM-dd")
    @Excel(name = "记录时间", width = 30, dateFormat = "yyyy-MM-dd")
    private Date recTime;

    /** 商家编码 */
    @Excel(name = "商家编码")
    private String partyCode;

    /** 商品条形码 */
    @Excel(name = "商品条形码")
    private String modelId;

    /** sku图片 */
    @Excel(name = "sku图片")
    private String pic;

    /** sku名称 */
    @Excel(name = "sku名称")
    private String skuName;

    /** 产品名称 */
    @Excel(name = "产品名称")
    private String prodName;

    /** 重量 */
    @Excel(name = "重量")
    private Long weight;

    /** 版本号 */
    @Excel(name = "版本号")
    private Long version;

    /** 商品体积 */
    @Excel(name = "商品体积")
    private Long volume;

    /** 状态（0 启用 1 禁用） */
    @Excel(name = "状态", readConverterExp = "0=,启=用,1=,禁=用")
    private String status;

    /** (0 正常 1 删除) */
    private String isDelete;
    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("skuId", getSkuId())
            .append("productId", getProductId())
            .append("properties", getProperties())
            .append("oriPrice", getOriPrice())
            .append("price", getPrice())
            .append("stocks", getStocks())
            .append("actualStocks", getActualStocks())
            .append("updateTime", getUpdateTime())
            .append("recTime", getRecTime())
            .append("partyCode", getPartyCode())
            .append("modelId", getModelId())
            .append("pic", getPic())
            .append("skuName", getSkuName())
            .append("prodName", getProdName())
            .append("weight", getWeight())
            .append("version", getVersion())
            .append("volume", getVolume())
            .append("status", getStatus())
            .append("isDelete", getIsDelete())
            .toString();
    }
}
