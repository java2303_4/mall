package com.ruoyi.product.domain;

import java.util.List;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.ruoyi.common.annotation.Excel;
import com.ruoyi.common.core.domain.BaseEntity;

/**
 * 规格管理对象 pro_specification
 * 
 * @author ruoyi
 * @date 2024-01-03
 */
public class Specification extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** 规格ID */
    private Long specId;

    /** 属性名称 */
    @Excel(name = "属性名称")
    private String propName;

    /** 属性值 */
    @Excel(name = "属性值")
    private String rule;

    /** 店铺ID */
    private Long shopId;

    private List<SpecificationProduct> specificationProductList;

    public List<SpecificationProduct> getSpecificationProductList() {
        return specificationProductList;
    }

    public void setSpecificationProductList(List<SpecificationProduct> specificationProductList) {
        this.specificationProductList = specificationProductList;
    }

    /** $table.subTable.functionName信息 */
    private List<ProSpecificationProduct> proSpecificationProductList;

    public void setSpecId(Long specId) 
    {
        this.specId = specId;
    }

    public Long getSpecId() 
    {
        return specId;
    }
    public void setPropName(String propName) 
    {
        this.propName = propName;
    }

    public String getPropName() 
    {
        return propName;
    }
    public void setRule(String rule) 
    {
        this.rule = rule;
    }

    public String getRule() 
    {
        return rule;
    }
    public void setShopId(Long shopId) 
    {
        this.shopId = shopId;
    }

    public Long getShopId() 
    {
        return shopId;
    }

    public List<ProSpecificationProduct> getProSpecificationProductList()
    {
        return proSpecificationProductList;
    }

    public void setProSpecificationProductList(List<ProSpecificationProduct> proSpecificationProductList)
    {
        this.proSpecificationProductList = proSpecificationProductList;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("specId", getSpecId())
            .append("propName", getPropName())
            .append("rule", getRule())
            .append("shopId", getShopId())
            .append("proSpecificationProductList", getProSpecificationProductList())
            .toString();
    }
}
