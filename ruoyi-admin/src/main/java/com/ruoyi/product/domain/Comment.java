package com.ruoyi.product.domain;

import java.util.Date;
import com.fasterxml.jackson.annotation.JsonFormat;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.ruoyi.common.annotation.Excel;
import com.ruoyi.common.core.domain.BaseEntity;

/**
 * 评论管理对象 pro_comment
 * 
 * @author ruoyi
 * @date 2024-01-03
 */
public class Comment extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** ID */
    private Long commentId;

    /** 商品名 */
    @Excel(name = "商品名")
    private String shopName;

    /** 用户昵称 */
    @Excel(name = "用户昵称")
    private String nickName;

    /** 记录时间 */
    @JsonFormat(pattern = "yyyy-MM-dd")
    @Excel(name = "记录时间", width = 30, dateFormat = "yyyy-MM-dd")
    private Date recordTime;

    /** 回复时间 */
    @JsonFormat(pattern = "yyyy-MM-dd")
    @Excel(name = "回复时间", width = 30, dateFormat = "yyyy-MM-dd")
    private Date replyTime;

    /** 评论得分 */
    @Excel(name = "评论得分")
    private Long commentScore;

    /** 是否匿名(0 不匿名 1 匿名 ) */
    @Excel(name = "是否匿名(0 不匿名 1 匿名 )")
    private String anonymity;

    /** 审核状态（0 未审核 1 审核通过 2 审核不通过） */
    @Excel(name = "审核状态", readConverterExp = "0=,未=审核,1=,审=核通过,2=,审=核不通过")
    private String status;

    /** 用户ID */
    private Long userId;

    /** 订单项ID */
    private Long orderItemId;

    /** 商品ID */
    private Long prodId;

    /** 评论内容 */
    private String content;

    /** 评论晒图 */
    private String pics;

    /** 掌柜回复 */
    private String replyContent;

    /** IP来源 */
    private String postIp;

    /** 评价（0 好评 1 中评 2 差评） */
    private Integer evaluate;

    public void setCommentId(Long commentId) 
    {
        this.commentId = commentId;
    }

    public Long getCommentId() 
    {
        return commentId;
    }
    public void setShopName(String shopName) 
    {
        this.shopName = shopName;
    }

    public String getShopName() 
    {
        return shopName;
    }
    public void setNickName(String nickName) 
    {
        this.nickName = nickName;
    }

    public String getNickName() 
    {
        return nickName;
    }
    public void setRecordTime(Date recordTime) 
    {
        this.recordTime = recordTime;
    }

    public Date getRecordTime() 
    {
        return recordTime;
    }
    public void setReplyTime(Date replyTime) 
    {
        this.replyTime = replyTime;
    }

    public Date getReplyTime() 
    {
        return replyTime;
    }
    public void setCommentScore(Long commentScore) 
    {
        this.commentScore = commentScore;
    }

    public Long getCommentScore() 
    {
        return commentScore;
    }
    public void setAnonymity(String anonymity) 
    {
        this.anonymity = anonymity;
    }

    public String getAnonymity() 
    {
        return anonymity;
    }
    public void setStatus(String status) 
    {
        this.status = status;
    }

    public String getStatus() 
    {
        return status;
    }
    public void setUserId(Long userId) 
    {
        this.userId = userId;
    }

    public Long getUserId() 
    {
        return userId;
    }
    public void setOrderItemId(Long orderItemId) 
    {
        this.orderItemId = orderItemId;
    }

    public Long getOrderItemId() 
    {
        return orderItemId;
    }
    public void setProdId(Long prodId) 
    {
        this.prodId = prodId;
    }

    public Long getProdId() 
    {
        return prodId;
    }
    public void setContent(String content) 
    {
        this.content = content;
    }

    public String getContent() 
    {
        return content;
    }
    public void setPics(String pics) 
    {
        this.pics = pics;
    }

    public String getPics() 
    {
        return pics;
    }
    public void setReplyContent(String replyContent) 
    {
        this.replyContent = replyContent;
    }

    public String getReplyContent() 
    {
        return replyContent;
    }
    public void setPostIp(String postIp) 
    {
        this.postIp = postIp;
    }

    public String getPostIp() 
    {
        return postIp;
    }
    public void setEvaluate(Integer evaluate) 
    {
        this.evaluate = evaluate;
    }

    public Integer getEvaluate() 
    {
        return evaluate;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("commentId", getCommentId())
            .append("shopName", getShopName())
            .append("nickName", getNickName())
            .append("recordTime", getRecordTime())
            .append("replyTime", getReplyTime())
            .append("commentScore", getCommentScore())
            .append("anonymity", getAnonymity())
            .append("status", getStatus())
            .append("userId", getUserId())
            .append("orderItemId", getOrderItemId())
            .append("prodId", getProdId())
            .append("content", getContent())
            .append("pics", getPics())
            .append("replyContent", getReplyContent())
            .append("postIp", getPostIp())
            .append("evaluate", getEvaluate())
            .toString();
    }
}
