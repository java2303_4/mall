package com.ruoyi.app;

import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.product.domain.Comment;
import com.ruoyi.product.service.ICommentService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import static com.ruoyi.common.core.domain.AjaxResult.success;

@RestController
@RequestMapping("/app/comment")
public class AppCommentController {
    @Autowired
    private ICommentService commentService;
    /**
     * 获取评论管理详细信息
     */
    @PreAuthorize("@ss.hasPermi('product:comment:query')")
    @GetMapping
    public AjaxResult getInfo(Comment comment)
    {
        return success(commentService.selectCommentList(comment));
    }
}
