import request from '@/utils/request'

// 查询用户配送地址列表
export function listAddr(query) {
  return request({
    url: '/outlet/addr/list',
    method: 'get',
    params: query
  })
}

// 查询用户配送地址详细
export function getAddr(addrId) {
  return request({
    url: '/outlet/addr/' + addrId,
    method: 'get'
  })
}

// 新增用户配送地址
export function addAddr(data) {
  return request({
    url: '/outlet/addr',
    method: 'post',
    data: data
  })
}

// 修改用户配送地址
export function updateAddr(data) {
  return request({
    url: '/outlet/addr',
    method: 'put',
    data: data
  })
}

// 删除用户配送地址
export function delAddr(addrId) {
  return request({
    url: '/outlet/addr/' + addrId,
    method: 'delete'
  })
}
