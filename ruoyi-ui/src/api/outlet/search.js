import request from '@/utils/request'

// 查询热搜管理列表
export function listSearch(query) {
  return request({
    url: '/outlet/search/list',
    method: 'get',
    params: query
  })
}

// 查询热搜管理详细
export function getSearch(hotSearchId) {
  return request({
    url: '/outlet/search/' + hotSearchId,
    method: 'get'
  })
}

// 新增热搜管理
export function addSearch(data) {
  return request({
    url: '/outlet/search',
    method: 'post',
    data: data
  })
}

// 修改热搜管理
export function updateSearch(data) {
  return request({
    url: '/outlet/search',
    method: 'put',
    data: data
  })
}

// 删除热搜管理
export function delSearch(hotSearchId) {
  return request({
    url: '/outlet/search/' + hotSearchId,
    method: 'delete'
  })
}
