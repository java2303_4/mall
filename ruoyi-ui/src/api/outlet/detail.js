import request from '@/utils/request'

// 查询店铺列表
export function listDetail(query) {
  return request({
    url: '/outlet/detail/list',
    method: 'get',
    params: query
  })
}

// 查询店铺详细
export function getDetail(shopId) {
  return request({
    url: '/outlet/detail/' + shopId,
    method: 'get'
  })
}

// 新增店铺
export function addDetail(data) {
  return request({
    url: '/outlet/detail',
    method: 'post',
    data: data
  })
}

// 修改店铺
export function updateDetail(data) {
  return request({
    url: '/outlet/detail',
    method: 'put',
    data: data
  })
}

// 删除店铺
export function delDetail(shopId) {
  return request({
    url: '/outlet/detail/' + shopId,
    method: 'delete'
  })
}
