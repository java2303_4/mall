import request from '@/utils/request'

// 查询主页轮播图列表
export function listImg(query) {
  return request({
    url: '/outlet/img/list',
    method: 'get',
    params: query
  })
}

// 查询主页轮播图详细
export function getImg(imgId) {
  return request({
    url: '/outlet/img/' + imgId,
    method: 'get'
  })
}

// 新增主页轮播图
export function addImg(data) {
  return request({
    url: '/outlet/img',
    method: 'post',
    data: data
  })
}

// 修改主页轮播图
export function updateImg(data) {
  return request({
    url: '/outlet/img',
    method: 'put',
    data: data
  })
}

// 删除主页轮播图
export function delImg(imgId) {
  return request({
    url: '/outlet/img/' + imgId,
    method: 'delete'
  })
}

export class AddPro {
}
