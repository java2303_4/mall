import request from '@/utils/request'


//搜索框下拉
export function myListArea(query) {
  return request({
    url: '/order/area/list',
    method: 'get',
    params: query
  })
}
// 查询地址管理列表
export function listArea(query) {
  return request({
    url: '/order/area/list',
    method: 'get',
    params: query
  })
}

// 查询地址管理详细
export function getArea(areaId) {
  return request({
    url: '/order/area/' + areaId,
    method: 'get'
  })
}

// 新增地址管理
export function addArea(data) {
  return request({
    url: '/order/area',
    method: 'post',
    data: data
  })
}

// 修改地址管理
export function updateArea(data) {
  return request({
    url: '/order/area',
    method: 'put',
    data: data
  })
}

// 删除地址管理
export function delArea(areaId) {
  return request({
    url: '/order/area/' + areaId,
    method: 'delete'
  })
}
