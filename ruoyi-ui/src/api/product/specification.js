import request from '@/utils/request'

// 查询规格管理列表
export function listSpecification(query) {
  return request({
    url: '/product/specification/list',
    method: 'get',
    params: query
  })
}

// 查询规格管理详细
export function getSpecification(specId) {
  return request({
    url: '/product/specification/' + specId,
    method: 'get'
  })
}

// 新增规格管理
export function addSpecification(data) {
  return request({
    url: '/product/specification',
    method: 'post',
    data: data
  })
}

// 修改规格管理
export function updateSpecification(data) {
  return request({
    url: '/product/specification',
    method: 'put',
    data: data
  })
}

// 删除规格管理
export function delSpecification(specId) {
  return request({
    url: '/product/specification/' + specId,
    method: 'delete'
  })
}
