// request.js
export function request(url,method,data) {
    return new Promise((resolve, reject) => {
        uni.request({
            url: 'http://192.168.8.248:8086'+url,
            data: data,
            method:method,
            success: (res) => {
                resolve(res.data);
            },
            fail: (err) => {
                reject(err);
            }
        });
    });
}
