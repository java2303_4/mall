import request from '@/utils/request'


export function mineOrderCount() {
	return request({
		url: '/app/mine/orderCount',
		method: 'get',
	})
}

export function mineCollection() {
	return request({
		url: '/app/mine/collectionCount',
		method: 'get',
	})
}

export function mineCollectionProd() {
	return request({
		url: '/app/mine/userCollectionList',
		method: 'get',
	})
}
export function mineCannelCollection(prodId) {
	return request({
		url: '/app/mine/userCannelCollection?prodId=' + prodId,
		method: 'get',
	})
}
export function isCollection(prodId) {
	return request({
		url: '/app/mine/isCollection?prodId=' + prodId,
		method: 'get',
	})
}
export function addOrderList(sts) {
	return request({
		url: '/app/mine/mineOrderList?status=' + sts,
		method: 'get',
	})
}
export function mineOrderList(orderNumber) {
	return request({
		url: '/app/mine/mineOrder?orderNumber=' + orderNumber,
		method: 'get',
	})
}
export function expressDelivery(orderNumber) {
	return request({
		url: '/app/mine/expressDelivery?orderNumber=' + orderNumber,
		method: 'get',
	})
}
export function mineUserAddrList() {
	return request({
		url: '/app/mine/userAddrList',
		method: 'get',
	})
}
export function getUserAddrInfo(addrId) {
	return request({
		url: '/app/mine/getUserAddrInfo?addrId=' + addrId,
		method: 'get',
	})
}
export function getAreaList(parentId) {
	return request({
		url: '/app/mine/getAreaList?parentId=' + parentId,
		method: 'get',
	})
}
export function deleteOrder(ordernum) {
	return request({
		url: '/app/mine/deleteOrder?orderNumber=' + ordernum,
		method: 'delete'
	})
}
export function deleteUserAddr(addrIds) {
	return request({
		url: '/app/mine/deleteUserAddr?addrIds=' + addrIds,
		method: 'delete'
	})
}
export function editStatus(ordernum, sts) {
	return request({
		url: '/app/mine/editStatus',
		method: 'put',
		data: {
			orderNumber: ordernum,
			status: sts
		}
	})
}
export function editUserAddr(method, url, data) {
	return request({
		url,
		method,
		data
	})
}
export function orderConfirm(data) {
	return request({
		url: '/app/mine/confirm',
		method: 'post',
		data: data
	})
}
export function editCommonAddr(addrId) {
	return request({
		url: '/app/mine/editCommonAddr',
		method: 'put',
		data: {
			commonAddr: 1,
			addrId: addrId
		}
	})
}