import request from '@/utils/request'

// 产品
export function productList(categoryId) {
    return request({
        url: '/app/product/list?categoryId='+categoryId,
        method: 'get',
    })
}