import request from '@/utils/request'

//商品信息
export function productDetails(productId) {
    return request({
        url: '/product/product/'+productId,
        method: 'get',
    })
}

//商品评论
export function comment(productId) {
    return request({
        url: '/app/comment?prodId='+productId,
        method: 'get',
    })
}