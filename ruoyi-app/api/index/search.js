import request from '@/utils/request'

//新闻
export function search() {
    return request({
        url: '/outlet/search/list',
        method: 'get',
    })
}