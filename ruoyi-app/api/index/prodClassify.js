import request from '@/utils/request'


//商品
export function goods(groupId) {
    return request({
        url: '/product/product/list?groupId='+groupId,
        method: 'get',
    })
}