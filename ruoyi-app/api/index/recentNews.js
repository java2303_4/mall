import request from '@/utils/request'

//新闻
export function news() {
    return request({
        url: '/outlet/notice/list',
        method: 'get',
    })
}