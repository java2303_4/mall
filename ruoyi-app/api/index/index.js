import request from '@/utils/request'

// 轮播
export function carouselChart() {
    return request({
        url: '/outlet/img/list',
        method: 'get',
    })
}

//新闻
export function news() {
    return request({
        url: '/outlet/notice/list',
        method: 'get',
    })
}


//商品标签
export function tag() {
    return request({
        url: '/product/group/list',
        method: 'get',
    })
}

//商品
export function goods(groupId) {
    return request({
        url: '/product/product/list?pageSize=9&groupId='+groupId,
        method: 'get',
    })
}