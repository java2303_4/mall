import request from '@/utils/request'

//新闻
export function news(id) {
    return request({
        url: '/outlet/notice/'+id,
        method: 'get',
    })
}