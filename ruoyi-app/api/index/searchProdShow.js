
import request from '@/utils/request'

//搜索
export function search(productName) {
    return request({
        url: '/product/product/list?productName='+productName,
        method: 'get',
    })
}