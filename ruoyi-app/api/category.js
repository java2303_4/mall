import request from '@/utils/request'

// 修改用户个人信息
export function categoryList(parentId) {
    return request({
        url: '/app/category/list?parentId='+parentId,
        method: 'get',
    })
}